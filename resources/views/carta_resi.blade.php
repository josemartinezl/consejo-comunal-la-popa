@extends('layouts.default')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Solicitar carta de residencia</div>
				<div class="panel-body">
					<form method="get" action="solicitud" class="navbar-form navbar-left pull-right" role="search">

					<div class="form-group">
					{!! Form::text('cedula',null,['class'=>'form-control','placeholder'=>'cedula del solicitante','required']) !!}
					</div>
					<div class="form-group">
					
					{!! Form::text('dirigido',null,['class'=>'form-control','placeholder'=>'Dirigido a','required']) !!}
					</div>
					<div class="form-group">
						{!! Form::text('motivo',null,['class'=>'form-control','placeholder'=>'Motivo','required']) !!}
					</div>
					<button type="submit" class="btn btn-default">Generar carta</button>

					</form>
				</div>
				@unless(!isset($mensaje))
					<div class="panel-heading">{{$mensaje}}</div>
				@endunless
			</div>
		</div>
	</div>
</div>