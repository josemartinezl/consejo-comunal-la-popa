@extends('layouts.default')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Agregar nuevo lider</div>
				<div class="panel-body">
					{!! Form::open(['route'=>'lider.index', 'method'=>'GET','class'=>'navbar-form navbar-left pull-right','role'=>'search']) !!}
					<div class="form-group">
					{!! Form::text('cedula',null,['class'=>'form-control','placeholder'=>'cedula del nuevo lider']) !!}
					</div>
					<div class="form-group">
					<select class="form-control" name="nombre">
						@foreach($vereda as $ver)
						<option value="{{$ver->id}}">{{$ver->nombre}}</option>
						@endforeach
					</select>
					<!--{!! Form::text('vereda',null,['class'=>'form-control','placeholder'=>'nombre de la vereda']) !!}-->
					</div>
					<button type="submit" class="btn btn-default">Agregar</button>
					{!! Form::close() !!}
				</div>
			</div>
			@unless(!isset($mensaje))
				<div class="panel-heading">{{$mensaje}}</div>
			@endunless
		</div>		 	
	</div>
</div>