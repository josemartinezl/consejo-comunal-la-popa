@extends('admin.template.main')
@section('title','Expedir carta de residencia')
@section('content')

<div class="col-md-10 col-md-offset-1">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2>
				Solicitar carta de residencia
			</h1>
		</div>
		<div class="panel-body">
			<form method="get" action="solicitud" role="search">

				<div class="form-group fix-form-group">
					{!! Form::label('cedula','Cedula del Habitante') !!}
					{!! Form::text('cedula',null,['class'=>'form-control','placeholder'=>'','required']) !!}
				</div>
				<div class="form-group fix-form-group">
					{!! Form::label('dirigido','Dirigida a') !!}
					{!! Form::text('dirigido',null,['class'=>'form-control','placeholder'=>'Dirigido a','required']) !!}
				</div>
				<div class="form-group fix-form-group">
				{!! Form::label('motivo','Morivo') !!}
					{!! Form::text('motivo',null,['class'=>'form-control','placeholder'=>'Motivo','required']) !!}
				</div>
				<button type="submit" class="btn btn-danger fix-bottom">Generar carta</button>

			</form>
		</div>
		@unless(!isset($mensaje))
		<div class="panel-heading">{{$mensaje}}</div>
		@endunless
	</div>
	</div>

@endsection