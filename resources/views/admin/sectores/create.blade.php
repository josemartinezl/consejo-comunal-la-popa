@extends('admin.template.main')

@section('title','Agregar Sector')

@section('content')

    <h1> Agregar sector </h1>

	{!! Form::open(['route' => 'sectores.store', 'method' => 'POST']) !!}

    <div class="form-group">
    	{!! Form::label('name','nombre') !!}
    	{!! Form::text('nombre',null,['class' => 'form-control', 'placeholder' => 'Nombre sector' , 'requiere']) !!}
     
    </div>

    <div>
    	{!! Form::submit('registrar', ['class' => 'btn btn-primary']) !!}
     
    </div>

	{!! Form::close() !!}

@endsection