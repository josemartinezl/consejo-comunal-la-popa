@extends('admin.template.main')

@section('title','Agregar Pais')

@section('content')

    <h1> Agregar Pais </h1>

	{!! Form::open(['route' => 'paises.store', 'method' => 'POST']) !!}

    <div class="form-group">
    	{!! Form::label('name','nombre') !!}
    	{!! Form::text('nombre',null,['class' => 'form-control', 'placeholder' => 'nombre pais' , 'requiere']) !!}
     
    </div>

    <div>
    	{!! Form::submit('registrar', ['class' => 'btn btn-primary']) !!}
     
    </div>

	{!! Form::close() !!}

@endsection