@extends('admin.template.main')
@section('content')
   
    <h1> Actualizar pais <?php echo $paises->nombre ?> </h1>
	
	{!! Form::open(['route' => ['paises.update',$paises->id], 'method' => 'PUT']) !!}

    <div class="form-group">
    	{!! Form::label('name','nombre') !!}
    	{!! Form::text('nombre',$paises->nombre,['class' => 'form-control', 'placeholder' => 'Nombre completo' , 'requiere']) !!}
     
    </div>

    <div>
    	{!! Form::submit('Actualizar', ['class' => 'btn btn-primary']) !!}
     
    </div>

	{!! Form::close() !!}
     
@endsection  

