@extends('admin.template.main')
@section('title','Listado de Personas')

@section('content')

<div class="col-md-10 col-md-offset-1">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2>
				Listado de Personas
			</h2>
		</div>
		<div class="row" style="margin-top: 10px; margin-bottom: 5px;">
			<div class="col-md-1"></div>
			<div class="col-md-4">
				<a href="{{ route('personas.create') }}" class="btn btn-danger" style="width: 100%"> Registar habitante</a>
			{!! Form::open(['route' => 'personas.index', 'method' => 'GET','class' => 'navbar-form']) !!}
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-1"></div>
			<div class="col-md-4">
				<div class="input-group">
					{!! Form::text('cedula',null,['class' => 'form-control','placeholder' => 'Buscar habitante por cedula', 'aria-describedby' => 'search']) !!}
					<span class="input-group-addon" id="search"> 
						<span class="glyphicon glyphicon-search" aria-hidden="true">
						</span>  
					</span>
				</div>
			</div>
			<div class="col-md-1"></div>
		</div>
		{!! Form::close() !!}

	</div>
		<table class="table table-striped">
			<thead>

				<th> Cedula </th>
				<th> Nombre </th>
				<th> Apellido </th>
				<th> Telefono </th>
				<th> Acción </th>
			</thead>
			<tbody>
				@foreach($personas as $persona)
				<tr>
					<td>{{ $persona->cedula}} </td>
					<td>{{ $persona->nombre}} </td>
					<td>{{ $persona->apellido}} </td>
					<td>{{ $persona->telefono}} </td>
					<td>    @if($persona->deleted_at==null)
                                                    <a href="{{ route('hab.edit', $persona->id) }}" class="btn btn-primary"> Editar</a>
                                                    <a href="{{ route('admin.hab.destroy', $persona->id) }}" onclick="return confirm('¿Seguro desea eliminar ? ' )" class="btn btn-danger"> Eliminar </a>
                                                @else
                                                      
                                                      <button  value="{{ $grupo->id }}" class="btn btn-primary" type="button" id="activar"  > Activar</button>


                                                @endif
					</td>
				</tr>
				@endforeach
			</tbody>

		</table>
		<div class="centrar" style="margin-left: 25%">
		{!! $personas->render() !!}
		</div>
</div>
@endsection