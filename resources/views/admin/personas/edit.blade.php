@extends('admin.template.main')
@section('title','Editar persona')

@section('content')
<div class="col-md-10 col-md-offset-1">
    <div class="panel panel-default">
        <div class="panel-heading">
          <h2>
            Modificar Habitante
        </h2>
    </div>
    {!! Form::open(['route' => ['hab.update',$persona->id], 'method' => 'PUT']) !!}

    
    <div class="form-group fix-form-group">  
        {!! Form::label('nombre','Nombre') !!}
        {!! Form::text('nombre',$persona->nombre,['class'=>'form-control','placeholder'=>'','required'])!!}
    </div>

    <div class="form-group fix-form-group"> 
        {!! Form::label('apellido','Apellido') !!}
        {!! Form::text('apellido',$persona->apellido,['class'=>'form-control','placeholder'=>'','required'])!!}
    </div>

    <div class="form-group fix-form-group"> 
        {!! Form::label('parentesco','Parentesco') !!}
        {!! Form::text('parentesco',$persona->parentesco,['class'=>'form-control','placeholder'=>'Parentesco familiar','required'])!!}
    </div>


    <div class="form-group fix-form-group"> 
        {!! Form::label('sexo','Sexo') !!}
        {!! Form::select('sexo',[''=>'Seleccion','0'=>'Femenino','1'=>'Masculino'], $persona->sexo,['class'=>'form-control','required']) !!}
    </div>

    <div class="form-group fix-form-group"> 
        {!! Form::label('fecha_nacimiento','Fecha Nacimiento') !!}
        <div class='input-append date datepicker'>
            {!! Form::text('fecha_nacimiento',$persona->fecha_nacimiento,['class'=>'form-control','placeholder'=>'Fecha Nacimiento','required'])!!}
            <span class="add-on"><i class="icon-th"></i></span>
        </div>
    </div>
    
    <div class="form-group fix-form-group"> 
        {!! Form::label('cedula','Cedula') !!}
        {!! Form::text('cedula',$persona->cedula,['class'=>'form-control','placeholder'=>'Cedula'])!!}
    </div>

    <div class="form-group fix-form-group"> 
        {!! Form::label('telefono','Teléfono') !!}
        {!! Form::text('telefono',$persona->telefono,['class'=>'form-control','placeholder'=>''])!!}
    </div>

    <div class="form-group fix-form-group"> 
        {!! Form::label('estado_civil','Estado Civil') !!}
        {!! Form::text('estado_civil',$persona->estado_civil,['class'=>'form-control','placeholder'=>'','required'])!!}
    </div>

    <div class="form-group fix-form-group"> 
        {!! Form::label('jefe_familiar','Jefe Familiar') !!}
        {!! Form::select('jefe_familiar',[''=>'Seleccion','1'=>'Jefe','0'=>'No Jefe'], $persona->jefe_familia,['class'=>'form-control']) !!}
    </div>

    <div class="form-group fix-form-group"> 
        {!! Form::label('ocupacion','Ocupacion') !!}
        {!! Form::text('ocupacion',$persona->ocupacion,['class'=>'form-control','placeholder'=>''])!!}
    </div>

    {!! Form:: hidden('grupo_familiar_id',$persona->grupo_familiar_id)!!}
    
    <div class="form-group fix-form-group"> 
        {!! Form::label('nivel_instruccion','Nivel de Instruccion') !!}
        {!! Form::text('nivel_instruccion',$persona->nivel_instruccion,['class'=>'form-control','placeholder'=>''])!!}
    </div>

    <div class="form-group fix-form-group">     
        {!! Form::label('enfermedad','Enfermedad') !!}
        {!! Form::text('enfermedad',$persona->enfermedad,['class'=>'form-control','placeholder'=>''])!!}
    </div>

    <div class="form-group fix-form-group">  

        {!! Form::label('paises','Pais ') !!}
        {!! Form::select('paises', $paises,$pais->id,['class' => 'form-control','placeholder'=>'Seleccionar','required']) !!}

    </div>

    <div class="form-group fix-form-group">   

         {!! Form::label('estados','Estado') !!}
        {!! Form::select('estados',[$estado->id => $estado->nombre,'placeholder'=>'Seleccionar Estado'],null,['class' => 'form-control','id'=>'estados']) !!}

    </div>
    
    <div>
        <div class="form-group fix-form-group">
        {!! Form::submit('Actualizar', ['class' => 'btn btn-danger fix-bottom']) !!}
       </div>
   </div>

   {!! Form::close() !!}

   @endsection  

</div>
</div>
@section('js')
<script>
    $(document).on('change','#paises',function(){

        var cat_id=$(this).val();

        var div=$(this).parent();

            //var url ="/servicio_comunitario/public/getveredas";
            var op=" ";
            $.ajax({

                type:'get',
                url:'{!!URL::to('/admin/getestados')!!}',
                data:{'id':cat_id},

                success:function(data){
                    console.log('success');
                    $('#estados').empty();
                    
                    for(var i=0;i<data.length;i++){
                        $('#estados').append(`<option value=${data[i].id}> ${data[i].nombre} </option>`);
                        console.log(data[i].id);
                        console.log(data[i].nombre);
                    }
                },
                error:function(){

                }
            });
        });
    </script>

    <script>
        $('.datepicker').datepicker({
            format: "dd-mm-yyyy",
            language: "es",
            autoclose: true
        });
    </script>
    @endsection

