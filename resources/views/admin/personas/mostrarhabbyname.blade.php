@extends('admin.template.main')
@section('title','Agregar persona')

@section('content')
    
    <div class="container">
    <div class="row">
        <div class="col-md-12">
                <div class="panel panel-default">
                  <table class="table table-striped table-condensed task-table">
                                        <thead>
                                            <th>Nombre </th>
                                            <th>Apellido </th>
                                            <th>Cedula</th>
                                        </thead>
                                <tbody>
                                    @foreach ($habitantes as $habitante)
                                   
                                        <tr>
                                            <td class="table-text">
                                                <div>{{ $habitante->nombre }}</div>
                                            </td>

                                            <td class="table-text">
                                                <div>{{ $habitante->apellido }}</div>
                                            </td>

                                            <td class="table-text">
                                                <div>{{ $habitante->cedula }}</div>
                                            </td>
                                            
                                            <td class="table-text">
                                                 <a href="{{ route('admin.crear_habitante',$habitante->grupo_familiar_id) }}" class="btn btn-info"> Registrar habitante </a>
                                            </td>
                            
                                            
                                        </tr>
                                        
                                    @endforeach
                                </tbody>
                </table>

            </div>
        </div>      
    </div>
    </div>

 
 
     


@endsection