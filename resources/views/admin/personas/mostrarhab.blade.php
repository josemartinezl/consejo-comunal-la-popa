@extends('admin.template.main')
@section('title','Agregar persona')

@section('content')
    
        <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                  
                    <div class="panel-heading">
                    @foreach ($familias as $familia)
                    <h3><b>Familia {{ $familia->nombre }} </b></h3>
                    @endforeach
                </div>
               
                <table class="table table-striped table-condensed task-table">
                                        <thead>
                                            <th>Nombre </th>
                                            <th>Apellido </th>
                                            <th>Cedula</th>
                                        </thead>
                                <tbody>
                                    @foreach ($grupo as $carg)
                                   
                                        <tr>
                                            <td class="table-text">
                                                <div>{{ $carg->nombre }}</div>
                                            </td>

                                            <td class="table-text">
                                                <div>{{ $carg->apellido }}</div>
                                            </td>

                                            <td class="table-text">
                                                <div>{{ $carg->cedula }}</div>
                                            </td>
                            
                                            
                                        </tr>
                                        
                                    @endforeach
                                </tbody>
                </table>

            </div>
        </div> 
    <div class="col-sm-5 col-sm-offset-2 col-md-6 col-md-offset-5">
                    @foreach ($familias as $familia)
                    <a href="{{ route('admin.crear_habitante',$familia->id) }}" class="btn btn-danger"> Registrar habitante </a>
                    @endforeach
        
    </div >
 
     


@endsection