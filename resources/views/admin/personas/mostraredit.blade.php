@extends('admin.template.main')
@section('title','Editar persona')

@section('content')
	
	<div class="container">
    <div class="row">
        <div class="col-md-12">
                <div class="panel panel-default">
                  
                    <div class="panel-heading">
                    @if($grupo->deleted_at!=null)
                        <strong> El habitante {{$grupo->nombre.' '.$grupo->apellido}} anteriormente pertenecio a la comunidad pero actualmente no vive en la parroquia </strong>
                    @endif
                    </div>
               
                <table class="table table-striped table-condensed task-table">
                                <thead>
                                            <th>Nombre y Apellido </th>
                                            <th>Cedula</th>
                                            <th>Accion </th>
                                </thead>
                                <tbody>
                                
                                   
                                        <tr>
                                            <td class="table-text" >
                                                <div>{{ $grupo->nombre.' '.$grupo->apellido }}</div>
                                            </td>

                                             <td class="table-text">
                                                <div>{{ $grupo->cedula }}</div>
                                            </td>
                                            <td>
                                                @if($grupo->deleted_at==null)
                                                    <a href="{{ route('hab.edit', $grupo->id) }}" class="btn btn-primary"> Editar</a>
                                                    <a href="{{ route('admin.hab.destroy', $grupo->id) }}" onclick="return confirm('¿Seguro desea eliminar ? ' )" class="btn btn-danger"> Eliminar </a>
                                                @else
                                                      
                                                      <button  value="{{ $grupo->id }}" class="btn btn-primary" type="button" id="activar"  > Activar</button>


                                                @endif
                                            </td>
                            
                                            
                                        </tr>
                                        
                                    
                                </tbody>
                </table>

            </div>
        </div>      
    </div>

    </div>

@include('admin.personas.activate-modal',['nombre'=>'jose'])
@endsection

@section('js')
<script type="text/javascript">

$('document').ready(function(){

        $('#activar').click(function(e) {

            //$('#comprobar').addClass("btn btn-primary").empty().append("Comprobar");
            $('#comprobar').hide();
            $('#myModalactivate').find(".titulo").empty();
            var id = $(this).val();
            
            $.ajax({

                type:'GET',
                url:'{!!URL::to('/admin/indexedit/activar')!!}',
                
                data:{'id':id },
                 
                success:function(data){
                    console.log('success');
                    /*
                    for(var i=0;i<data.length;i++){
                        console.log(data[i].cedula);
                        console.log(data[i].nombre);
                        }
                    */

                    $('#myModalactivate').modal('show');
                    id = $('#myModalactivate').find(".titulo").append( "Agregar a " + data[0].nombre + " " + data[0].apellido + " nuevamente a la comunidad");
                    $('.info').empty().append("Para reincorporar a " +  data[0].nombre + " " +  data[0].apellido + " a la comunidad ingrese el numero de cedula de algun miembro de un grupo familiar al cual va a pertenecer");
                    
                    
                  
                },
                error:function(){
                    console.log('no sirve ajax opcional');
              }
            });  
            });

        $('#parrafo').keypress(function(e) {
            if(e.which == 13) {
                $('#comprobar').hide();
                $('#mensajecheck').empty();
                $('#persona').empty();
                var cedula = $('#parrafo').find("input").val();
                //$(this).html("<i class='fa fa-spinner fa-spin'></i>&nbsp;Wait!").attr('disabled', true);
                //var texto =   $('#comprobar').text();
                $.ajax({

                    type:'GET',
                    url:'{!!URL::to('/admin/indexedit/comprobar')!!}',
                    data:{'cedula':cedula,
                          'tipo':'comprobar'                 },
                 
                    success:function(data){
                  
                                if(data.length == 0){
                                    $('#mensajecheck').empty().append("No existe").css( "color", "red" ).show();
                                    // $("h1, h2, p").addClass("blue");
                                    //$('#comprobar').addClass("btn btn-warning").empty().append("No existe");
                                    console.log('no existe');
                                }else{
                                    $('#persona').empty().append("El numero de cedula pertence a "+ data[0].nombre + " " + data[0].apellido).show();
                                    //$('#mensajecheck').empty().append("existe").css( "color", "green" ).show();
                                    $('#comprobar').addClass("btn btn-success").empty().append("Aceptar").show();

                                    // console.log(valor);
                                    console.log('existe');
                    }    

                   
                  
                },
                error:function(){
                    console.log('no sirve ajax comprobar');
              }
            }); 

            //$('#comprobar').show();            

             console.log( "Handler for .keypress() called." );
             }
            
        });
        

        $('#comprobar').click(function(e) {
            

            var cedula = $('#parrafo').find("input").val();
            var id = $('#activar').val();
            console.log(id);
            //var texto =   $('#comprobar').text();
            //console.log(cedula);
            // if (texto == "Comprobar"){
            $.ajax({

                type:'GET',
                url:'{!!URL::to('/admin/indexedit/comprobar')!!}',
                data:{'cedula':cedula,
                      'tipo':'update',
                      'id':id },
                 
                success:function(data){
                    console.log('probando habilitacion');
                    console.log(data);
                  
                 

                   
                  
                },
                error:function(){
                    console.log('no sirve ajax habilitador');
              }
            });  
           // }




              });
      });




    
</script>


@endsection

