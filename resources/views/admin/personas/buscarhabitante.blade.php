@extends('admin.template.main')
@section('title','Agregar persona')

@section('content')
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h2>
            Buscar miembro del grupo familiar
          </h2>
        </div>
        <div class="panel-body">
         <p class="text-info">Ingresar la cedula de algun habitante que pertenezca al grupo familiar para poder registrar un nuevo habitante. Debe indicar el tipo de ciudadano si posee alguno. Ejemplo: V10000000. Los tipos de ciudadanos son venezolano: (V),extranjero(E) y si posee pasaporte(P).</p>  

         <div class="form-group">
          {!! Form::open(['route'=>'hab.index', 'method'=>'GET','class'=>'navbar-form','role'=>'search']) !!}
          <div class="form-group centrar">
            {!! Form::text('nombre',null,['class'=>'form-control','placeholder'=>'Cedula o nombre']) !!}
          </div>
          <button type="submit" class="btn btn-danger">Ir</button>
          {!! Form::close() !!}
        </div>
                       <!-- <p>
                        <a class="btn btn-info" href="{{ route('hab.create') }}" role="button">Nuevo</a>
                      </p> -->
                    </div>
                  </div>
                </div>



            @endsection