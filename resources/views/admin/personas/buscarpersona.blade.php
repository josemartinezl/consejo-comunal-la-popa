@extends('admin.template.main')
@section('title','Editar/eliminar persona')

@section('content')
<div class="col-md-10 col-md-offset-1">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h2>
        Buscar Habitante
      </h2>
    </div>
    <div class="panel-body">
      <div class="form-group">
        {!! Form::open(['route'=>'admin.index_edit', 'method'=>'GET','class'=>'navbar-form','role'=>'search']) !!}
        <div class="form-group centrar">
          {!! Form::text('nombre',null,['class'=>'form-control','placeholder'=>'Cedula o Nombre']) !!}
        </div>

        <button type="submit" class="btn btn-danger">Ir</button>
        {!! Form::close() !!}
      </div>
                       <!-- <p>
                        <a class="btn btn-info" href="{{ route('hab.create') }}" role="button">Nuevo</a>
                      </p> -->
                    </div>
                  </div>
                </div>
                @endsection