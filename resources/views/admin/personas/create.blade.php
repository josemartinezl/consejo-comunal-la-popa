@extends('admin.template.main')

@section('title','Agregar Persona')

@section('content')

    <h1> Agregar Persona </h1>

	{!! Form::open(['route' => 'personas.store', 'method' => 'POST']) !!}

    <div class="form-group">
    	{!! Form::label('ci','cedula') !!}
    	{!! Form::number('cedula',null,['class' => 'form-control' , 'requiere']) !!}
     
    </div>
    
    <div class="form-group">
        {!! Form::label('name','Nombres') !!}
        {!! Form::text('nombre',null,['class' => 'form-control' , 'requiere']) !!}
     
    </div>
      
    <div class="form-group">
        {!! Form::label('last_name','Apellidos') !!}
        {!! Form::text('apellido',null,['class' => 'form-control' , 'requiere']) !!}
     
    </div>

    <div class="form-group">
        {!! Form::label('parentesco','Parentesco') !!}
        {!! Form::text('parentesco',null,['class' => 'form-control' , 'requiere']) !!}
     
    </div>

     <div class="form-group">
        {!! Form::label('telefeno','Telefono') !!}
        {!! Form::text('telefono',null,['class' => 'form-control' , 'requiere']) !!}
     
    </div>

    <div class="form-group">
        {!! Form::label('estado_civil','Estado Civil') !!}
        {!! Form::select('estado_civil',['soltero' => 'Soltero', 'casado' => 'Casado','divorciado' => 'Divorciado', 'viudo' => 'Viudo' ],null,['class' => 'form-control','placeholder' => 'Seleccione su estado civil','required']) !!}
       
    </div>

    <div class="form-group">
        {!! Form::label('pais_id','Lugar de nacimento') !!}
        {!! Form::select('pais_id',$paises,null,['class' => 'form-control','placeholder' => 'Seleccione un pais','required']) !!}
       
    </div>


    <div class="form-group">
        {!! Form::label('sex','Sexo') !!}
        {!! Form::select('sexo',['0' => 'Femenino', '1' => 'Masculino'],null,['class' => 'form-control','placeholder' => 'Seleccione genero','required']) !!}
       
    </div>

    <div class="form-group">
        {!! Form::label('grupo_familiar_id','Grupo familiar') !!}
        {!! Form::select('grupo_familiar_id',$grupos,null,['class' => 'form-control','required']) !!}
       
    </div>

    <div>
    	{!! Form::submit('registrar', ['class' => 'btn btn-primary']) !!}
     
    </div>

	{!! Form::close() !!}

@endsection