@extends('admin.template.main')
@section('title','Listado de Personas')

@section('content')

<div class="col-md-10 col-md-offset-1">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2>
				Resultado de busqueda
			</h2>
		</div>
		<div class="row" style="margin-top: 10px; margin-bottom: 5px;">
			<div class="col-md-1"></div>
			<div class="col-md-4">
				
			
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-1"></div>
			<div class="col-md-4">
			
			</div>
			<div class="col-md-1"></div>
		</div>
		

	</div>
		<table class="table table-striped">
			<thead>

				<th> Cedula </th>
				<th> Nombre </th>
				<th> Apellido </th>
				<th> Telefono </th>
				<th> Acción </th>
			</thead>
			<tbody>
				@foreach($personas as $persona)
				<tr>
					<td>{{ $persona->cedula}} </td>
					<td>{{ $persona->nombre}} </td>
					<td>{{ $persona->apellido}} </td>
					<td>{{ $persona->telefono}} </td>
					<td>    @if($persona->deleted_at==null)
                                                    <a href="{{ route('hab.edit', $persona->id) }}" class="btn btn-primary"> Editar</a>
                                                    <a href="{{ route('admin.hab.destroy', $persona->id) }}" onclick="return confirm('¿Seguro desea eliminar ? ' )" class="btn btn-danger"> Eliminar </a>
                                                @else
                                                      
                                                      <button  value="{{ $grupo->id }}" class="btn btn-primary" type="button" id="activar"  > Activar</button>


                                                @endif
					</td>
				</tr>
				@endforeach
			</tbody>

		</table>
		<div class="centrar" style="margin-left: 25%">
		{!! $personas->render() !!}
		</div>
</div>
@endsection