@extends('admin.template.main')
@section('title','Listado Especial')


@section('content')
<div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h2>
           Jornadas de Clap
          </h2>
        </div>
    	<table class="table table-striped">
		<thead>
			<th> Jornada </th>
			<th> Acción </th>
			
		

		</thead>
		<tbody>
		
			@foreach($jornadas as $jornada)
			<tr>
				<td>  <a href="{{ route('admin.listadosclap.show', $jornada->fecha) }}"> {{ $jornada->fecha }} </a>  </td>
				<td>  <a href="{{ route('admin.listadosclap.destroy', $jornada->fecha) }}" onclick="return confirm('¿Seguro quiere eliminar la jornada del clap con fecha del {{ $jornada->fecha }} ' )" class="btn btn-danger"> Eliminar </a> </td>
					
				
			
			</tr>
			@endforeach
		</tbody>
		
	</table>
		{{ $jornadas->render() }}



@endsection