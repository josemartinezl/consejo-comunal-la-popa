@extends('admin.template.main')
@section('title','Listado Especial')


@section('content')
<div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h2>
            Jornada de CLAP con fecha {{ $fecha }}
          </h2>
        </div>
    
    	{!! Form::open(['route' => ['admin.listadosclap.show',$fecha], 'method' => 'GET','class' => 'navbar-form pull-right']) !!}
		<div class="input-group">
			{!! Form::text('nombre',null,['class' => 'form-control','placeholder' => 'Buscar familia...', 'aria-describedby' => 'search']) !!}
			<span class="input-group-addon" id="search"> 
            	<span class="glyphicon glyphicon-search" aria-hidden="true">
            	</span>  
			</span>
			
		</div>
			
	{!! Form::close() !!}

    	<table class="table table-striped">
		<thead>
			<th> Familia </th>
			<th> Dirección </th>
			<th> Vereda </th>
			<th> Estado </th>
			<th> Cambiar </th>		

		</thead>
		<tbody>
		
			@foreach($jornadas as $jornada)
			<tr>
				<td> {{ $jornada->familia }}  </td>
				<td> {{ $jornada->direccion }}  </td>
				<td> {{ $jornada->vereda }}  </td>
				@if ($jornada->estado === 0)
				<td> <font color="red"> <strong> No entregado </strong> </font> </td>
				@else
				<td> <font color="blue"> <strong> Entregado </strong> </font>   </td>
				@endif

				<td>  <a href="{{ route('admin.listadosclap.edit', $jornada->id) }}" onclick="return confirm('¿Cambiar estado de la familia ? {{ $jornada->familia }} ' )" class="btn btn-primary"></a> </td>
			
				
			
			</tr>
			@endforeach
		</tbody>
		
	</table>

</div>
</div>


@endsection