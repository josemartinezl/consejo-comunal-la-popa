@extends('admin.template.main')
@section('title','Listado Especial')

@section('content')
<div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h2>
            Agregar Jornada de Entrega del CLAP
          </h2>
        </div>

    {!! Form::open(['route' => 'listadosclap.store', 'method' => 'POST']) !!}

    <div class="form-group fix-form-group">
        {!! Form::label('name','Fecha de jornada') !!}
        {!! Form::text('fecha', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control']) !!} 
 
    </div>

    <div class="form-group fix-form-group">

        {!! Form::label('mercado','Tipo de Mercado') !!}
        {!! Form::text('mercado',null,['class' => 'form-control', 'placeholder' => 'PDVAL, Mercal, otros' , 'requiere']) !!}
     
    </div>        

    <div class="form-group fix-form-group">
        {!! Form::submit('Crear Jornada', ['class' => 'btn btn-danger fix-bottom']) !!}
     
    </div>

    {!! Form::close() !!}

    @section('js')
    
    <script type="text/javascript">
        
        $('#datepicker').datepicker({
            format: "dd-mm-yyyy",
            language: "es",
            autoclose: true
        });
    </script>
    
</div>
</div>
    @endsection

@endsection