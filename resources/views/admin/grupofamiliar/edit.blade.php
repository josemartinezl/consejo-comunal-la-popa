@extends('admin.template.main')
@section('title','Editar grupo familiar')

@section('content')
<div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h2>
            Actualizar familia {{$grupos[0]->nombre }}
          </h2>
        </div>
    
    {!! Form::open(['route' => ['grupofamiliar.update',$grupos[0]->id], 'method' => 'PUT']) !!}
        
    <div class="form-group fix-form-group">
        {!! Form::label('vereda_ID','Vereda') !!}
        {!! Form::select('vereda_ID',[''=>'Seleccionar','1'=>'0','2'=>'1','3'=>'2','4'=>'3','5'=>'4','6'=>'5','7'=>'Via Principal Granjas Infatiles','8'=>'Sector Escuela','9'=>'6','10'=>'6 Bis','11'=>'7','12'=>'7 Bis','13'=>'7 Bis Camino Real y 8 Bis','14'=>'8','15'=>'Via Principal Los Tanques','16'=>'Sector Los Tanques'],$grupos[0]->vereda_ID,['class'=>'form-control','required']) !!}
    </div>
   
    <div class="form-group fix-form-group">
        {!! Form::label('nombre','Nombre de Grupo Familiar') !!}
        {!! Form::text('nombre',$grupos[0]->nombre,['class'=>'form-control','placeholder'=>'','required'])!!}
    </div>
    
    <div class="form-group fix-form-group">
        {!! Form::label('direccion','Direccion de vivienda') !!}
        {!! Form::text('direccion',$grupos[0]->direccion,['class'=>'form-control','placeholder'=>'','required'])!!}
    </div>
    <div class="form-group fix-form-group">
        {!! Form::submit('Actualizar', ['class' => 'btn btn-danger fix-bottom']) !!}
    </div>
    {!! Form::close() !!}
     </div>
     </div>
@endsection  

