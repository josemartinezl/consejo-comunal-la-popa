@extends('admin.template.main')
@section('title','Buscar grupo familiar ')
@section('content')

<div class="container">
  <div class="col-md-10 col-md-offset-1">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h2>
          Buscar Grupo Familiar
        </h2>
      </div>
      <div class="panel-body">
       <p class="text-info">Ingresar la cedula de algun habitante que pertenezca al grupo familiar para poder visualizar los integrantes de la familia.</p>  

       <div class="form-group">
        {!! Form::open(['route'=>'grupofamiliar.index', 'method'=>'GET','class'=>'navbar-form','role'=>'search']) !!}
        <div class="form-group centrar">
          {!! Form::text('nombre',null,['class'=>'form-control','placeholder'=>'Cedula o nombre']) !!}
        </div>
        <button type="submit" class="btn btn-danger">Ir</button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
</div>
</div>
@endsection