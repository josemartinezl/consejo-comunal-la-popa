@extends('admin.template.main')
@section('title','Editar/Eliminar grupo familiar')

@section('content')
<div class="col-md-10 col-md-offset-1">
  <div class="panel panel-default">
    <div class="panel-heading">
      @if( $familias[0]->deleted_at!=null )
                        <strong> El grupo familiar {{$familias[0]->nombre}} anteriormente pertenecio a la comunidad pero actualmente no viven en la parroquia </strong>
                    @endif
      <h2>
       Resultados de Busqueda
   </h2>
</div>
<table class="table table-striped table-condensed task-table">



    <thead>

        <th> Familia: {{ $familias[0]->nombre}} </th>
        <th> Direccion:  {{ $familias[0]->direccion }} </th>
        <th> Vereda: {{ $vereda[0]->nombre }} </th>
        <th> 
            @if($familias[0]->deleted_at==null)
                                                <a style="height: 25px;
            padding: initial;
            width: auto;" href="{{ route('grupofamiliar.edit', $familias[0]->id) }}" class="btn btn-primary"> Editar</a>
                                                <a  style="height: 25px;
            padding: initial;
            width: auto;" href="{{ route('grupofamiliar.destroy',$familias[0]->id) }}" onclick="return confirm('al eliminar la familia tambien se elimina de la comunidad a sus integrantes ¿ Desea eliminar la familia ? ' )" class="btn btn-danger"> Eliminar </a>
            @else
                    <a  style="height: 25px;
            padding: initial;
            width: auto;" href=" {{ route('grupofamiliar.activate',$familias[0]->id) }} " onclick="return confirm('¿ACtivar grupo familiar  ? ' )" class="btn btn-primary"> Activar </a>
                    @endif
          
        </th>
    </thead>

</table>


<table class="table table-striped table-condensed task-table">

  <thead>
    <th>Nombre </th>
    <th>Apellido </th>
    <th>Cedula</th>
</thead>
<tbody>
    @foreach ($grupo as $carg)

    <tr>
        <td class="table-text">
            <div>{{ $carg->nombre }}</div>
        </td>

        <td class="table-text">
            <div>{{ $carg->apellido }}</div>
        </td>

        <td class="table-text">
            <div>{{ $carg->cedula }}</div>
        </td>


    </tr>

    @endforeach
</tbody>
</table>

</div>
</div>      

</div>





@endsection