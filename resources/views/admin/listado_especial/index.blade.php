  @extends('admin.template.main')
  @section('title','Listado Especial')
  @section('content')
  <div class="col-md-10 col-md-offset-1">
  <div class="panel panel-default">
  <div class="panel-heading">
    <h2> Listado Especial </h2>
  </div>
  {!! Form::open(['route' => 'listado_especial.index', 'method' => 'GET']) !!}
  <div class="fix-form-group">
    <div class="form-group">
      {!! Form::label('estado_civil','Generacion ') !!}
      {!! Form::select('generacion',['niños' => 'Menores de edad', 'adultos' => 'Mayores de edad','abuelos' => 'Mayores de 60 años'],null,['class' => 'form-control','placeholder' => 'Seleccione una busqueda de generacion ','required']) !!}
    </div>
    <div class="form-group">
     {!! Form::label('parroquias','Parroquia') !!}
     {!! Form::select('parroquias', $parroquias,null,['class' => 'form-control','placeholder' => 'Seleccionar parroquia ','required']) !!}
   </div>
   <div class="form-group"> 
    {!! Form::label('vereda','Vereda ') !!}
    @if ($vereda === null)
    {!! Form::select('veredas',['placeholder'=>'Seleccionar vereda'],null,['id'=>'veredas']) !!}
    @else
    {!! Form::select('veredas',$vereda ,null,['class'=>'form-control','placeholder'=>'Vereda','required','id'=>'veredas']) !!}
    @endif
  </div>
  </div>
  <div>
  <div class="form-group">
    {!! Form::submit('Buscar', ['class' => 'fix-bottom btn btn-danger']) !!}
    </div>
  </div>  
  </div>  
  <table class="table table-striped">
    <thead>
     <th> Cedula </th>
     <th> Nombre </th>
     <th> Apellido </th>
     <th> Fecha de Nacimiento</th>
     <th> Edad </th>
   </thead>
   <tbody>
     @foreach($paginado as $person)
     <tr>
      <td> {{ $person['cedula'] }} </td>
      <td> {{ $person['nombre'] }} </td>
      <td> {{ $person['apellido'] }} </td>
      <td> {{ $person['fecha_nacimiento'] }} </td>
      <td> {{ $person['edad'] }} </td>
    </tr>
    @endforeach
  </tbody>
  </table>
  <div class="centrar">
  {{ $paginado->render() }}
  {!! Form::close() !!}
  </div>
  </div>
  @endsection
  @section('js')
  <script type="text/javascript">
  /* codigo que funciona
  $('#parroquia').on('change', function(){
    console.log('hh');
     var url ="/sapu/public/admin";

      $.ajax({
           

          
          url: url +'/select2-autocomplete-ajax',
      
          success: function(){
              alert('succes');
          },
          error: function(data){
             // console.log(data);
              alert("fail" + ' ' + this.data)
          },

      });
  });
  */
  $(document).on('change','#parroquias',function(){
              // console.log("hmm its change");
              var cat_id=$(this).val();
              var div=$(this).parent();
              var url ="/sapu/public/admin/getveredas";
              var op=" ";
              $.ajax({
                type:'get',
                url:'{!!URL::to('/admin/getveredas')!!}',
                data:{'id':cat_id},
                  //dataType:'json',
                  success:function(data){
                    console.log('success');
                    $('#veredas').empty();
                    if (data == 3){
                      console.log('se metio en la puta comunidad');
                      $('#veredas').append(`<option > Todas las veredas </option>`);
                    }else{
                     for(var i=0;i<data.length;i++){
                       $('#veredas').append(`<option value=${data[i].id}> ${data[i].nombre} </option>`);
                     }
                   }
                 },
                 error:function(){
                 }
               });
            });
          </script>
          @endsection