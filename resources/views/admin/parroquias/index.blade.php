@extends('admin.template.main')

@section('title','Lista de parroquias')

@section('content')
	
	<h1>Lista de Parroquias </h1>
	<a href="{{ route('parroquias.create') }}" class="btn btn-info"> Registar Parroquia</a>
	<table class="table table-striped">
		<thead>
			<th> ID </th>
			<th> Nombre </th>
		</thead>
		<tbody>
			@foreach($parroquias as $parroquia)
			<tr>
				<td>{{ $parroquia->id}} </td>
				<td>{{ $parroquia->nombre}} </td>
				<td> <a href="{{ route('admin.parroquias.destroy', $parroquia->id) }}" onclick="return confirm('¿Seguro quiere eliminar la parroquia  <?php echo $parroquia->nombre ?> ?' )" class="btn btn-danger"></a>
					 <a href="{{ route('parroquias.edit', $parroquia->id) }}" class="btn btn-warning"></a></td>
			</tr>
			@endforeach
		</tbody>
		
	</table>
	{!! $parroquias->render() !!}
@endsection	