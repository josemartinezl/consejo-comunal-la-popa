@extends('admin.template.main')
@section('content')
   
    <h1> Actualizar Parroquia  <?php echo $parroquia->nombre ?> </h1>
	
	{!! Form::open(['route' => ['parroquias.update',$parroquia->id], 'method' => 'PUT']) !!}

    <div class="form-group">
    	{!! Form::label('name','nombre') !!}
    	{!! Form::text('nombre',$parroquia->nombre,['class' => 'form-control', 'placeholder' => 'Nombre completo' , 'requiere']) !!}
     
    </div>

    <div>
    	{!! Form::submit('Actualizar', ['class' => 'btn btn-primary']) !!}
     
    </div>

	{!! Form::close() !!}
     
@endsection  

