<!DOCTYPE html>
<html lang="en">
<head>
	 <meta charset="utf-8">
	 <title> @yield('title','Default') | Panel de Administracion </title>
	 <link rel="stylesheet"  href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}" >
     <link href="{{ asset('css/estilo.css') }}" rel="stylesheet">
	 <link rel="stylesheet" href="{{asset('plugins/datepicker/css/bootstrap-datepicker3.css')}}">
     <link rel="stylesheet" href="{{asset('plugins/datepicker/css/bootstrap-standalone.css')}}">
    
  	
</head>

<body>
<div class="container">
   	@include('admin.template.partials.header')
    @include('admin.template.partials.nav')
    
	<section>
		@include('flash::message')
		@include('admin.template.partials.errors')
		@yield('content')
	</section>
    </div>
	<script src="{{ asset('plugins/jquery/js/jquery-3.1.1.js') }}"></script>
	<script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>
	<script src="{{asset('plugins/datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('plugins/datepicker/locales/bootstrap-datepicker.es.min.js')}}"></script>

	<script src="{{ asset('js/script.js') }}"></script>

	 
	@yield('js')

	
</body>
</html