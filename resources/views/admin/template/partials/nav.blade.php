<nav class="navbar navbar-default">
  <div class="container-fluid">

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      @if(Auth::user())
      <ul class="nav navbar-nav">
        <li><a href="{{ route('home.index') }}">Inicio <span class="sr-only">(current)</span></a></li>
        <!--<li><a href="{{ route('parroquias.index') }}">Parroquias</a></li>
        <li><a href="{{ route('paises.index') }}">Paises</a></li>
        <li><a href="{{ route('personas.index') }}">Personas</a></li>-->
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Consejo Comunal<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li class="dropdown dropdown-submenu"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Habitante</a>
              <ul class="dropdown-menu">
               <li><a href="{{ route('admin.buscarhabitante') }}">Registrar Habitante</a></li>
               <li><a href="{{ route('admin.buscarpersona') }}">Modificar/Eliminar Habitante</a></li>
               <li><a href="{{ route('personas.index') }}">Listado de Habitantes</a></li>
             </ul>
           </li>
           <li class="dropdown dropdown-submenu"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Grupo Familiar</a>
            <ul class="dropdown-menu">
              <li><a href="{{ route('grupofamiliar.create')}}">Registrar Grupo Familiar</a></li>
              <li><a href="{{ route ('grupofamiliar.buscar')}}">Modificar/Eliminar Grupo Familiar</a></li>
            </ul>
          </li>
          <li role="separator" class="divider"></li>
          <li class="dropdown dropdown-submenu"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Lideres de Calle</a>
            <ul class="dropdown-menu">
              <li><a href="{{ route('lideres.create')}}">Agregar Lider de calle </a></li>
              <li><a href="{{ route('lideres.index')}}"> Listado de lideres de calle </a></li>
              <li><a href="{{ route('lideres.find')}}"> Buscar lider de calle </a></li>
            </ul>
          </li>
          <li role="separator" class="divider"></li>
           <li><a href=" {{ route('carta_resi.index') }}">Carta de Residencia</a></li>
          <li role="separator" class="divider"></li>
          <li><a href="{{ route('listado_especial.index')}}">Busqueda Avanzada</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">CLAP<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li> <a href="{{ route('listadosclap.create') }}">Agregar CLAP</a></li>
          <li> <a href="{{ route('listadosclap.index') }}" >Jornada de CLAP</a> </li>
        </ul>
      </li>
      <!--<li><a href="#">CDI <span class="sr-only">(current)</span></a></li> -->
    </ul>

    <form class="navbar-form navbar-right" action="{{ route('admin.resultsearch') }}" method="get">
      <div class="form-group">
        <input type="text" name="cedula" class="form-control" placeholder="Buscar por C.I.">
     </div>
     <button type="submit" class="btn btn-danger">Buscar</button>
   </form>


   <ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
      <ul class="dropdown-menu">
        <li>
          <a href="{{ route('logout') }}"
          onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">
          Salir
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
        </form>
      </li>
    </ul>
  </li>
</ul>
@endif
</div><!-- /.navbar-collapse -->
</div><!-- /.container-fluid -->
</nav>