@extends('admin.template.main')
@section('title','Buscar lider')

@section('content')
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				@foreach($lider_ac as $lider)
				<div class="panel-heading">
				<h2>Lider actual de {{$lider->vereda}}</h2>
			</div>
				@endforeach
				<table class="table">
					<thead>
					<tr>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Cedula</th>
					</tr>
				</thead>
				@foreach($lider_ac as $lider)
					<tr>
						<td>{{$lider->nombre}}</td>
						<td>{{$lider->apellido}}</td>
						<td>{{$lider->cedula}}</td>
					</tr>
				@endforeach
				</table>
			</div>
		</div>		 

@endsection