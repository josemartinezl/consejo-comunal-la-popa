@extends('admin.template.main')
@section('title','Buscar lider')


@section('content')
<div class="col-md-10 col-md-offset-1">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2>
				Listado de Lideres de Calle
			</h2>
		</div>
				<div class="panel-body">
					<form action="lideres_listado" class="" role="search">
						<button type="submit" class="btn btn-danger fix-bottom">Ver lista de lideres</button>
					</form>
				</div>
				</div>
				<div class="panel panel-default">
		<div class="panel-heading">
			<h2>
				Lider de Calle por Vereda
			</h2>
		</div>
				<div class="panel-body">
					<form action="lideres" class="" role="search">
					<div class="row">
					<div class="col-md-8">
						<select name="nombre" class="form-control">
							<option value="0">Seleccione</option>
							@foreach($veredas as $vereda)
							<option value="{{$vereda->nombre}}">{{$vereda->nombre}}</option>
							@endforeach
						</select>
						</div>
						<div class="col-md-4">
						<button type="submit" class="btn btn-danger" style="width: 100%">Ver lista de lideres</button>
						</div>
						</div>
					</form>
				</div>
			</div>
		</div>		 	
	</div>
</div>

@endsection