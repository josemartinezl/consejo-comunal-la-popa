@extends('admin.template.main')
@section('content')
   
    <h1> Agregar lider de calle </h1>
	
 


	{!! Form::open(['route' => 'lideres_calle.store', 'method' => 'POST']) !!}


    <div class="form-group">
        {!! Form::label('ci','cedula') !!}
        {!! Form::number('cedula',null,['class' => 'form-control' , 'requiere']) !!}
     
    </div>

     <div class="form-group">
        {!! Form::label('familias','Numero de familias') !!}
        {!! Form::number('numeros_familias',null,['class' => 'form-control' , 'requiere']) !!}
     
    </div>

    <div>
    	{!! Form::submit('registrar', ['class' => 'btn btn-primary']) !!}
     
    </div>

	{!! Form::close() !!}
     
@endsection  

