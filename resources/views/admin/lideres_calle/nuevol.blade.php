@extends('admin.template.main')
@section('title','Agregar Lider de Calle')

@section('content')
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">
          <h2>
            Agregar Lider De Calle
          </h2>
        </div>
				<div class="panel-body">
				<div class="form-group fix-form-group">
				<div class="row">
					{!! Form::open(['route'=>'lider.index', 'method'=>'GET','class'=>'','role'=>'search']) !!}
					<div class="form-group fix-form-group">
					{!! Form::text('cedula',null,['class'=>'form-control','placeholder'=>'Cedula del Nuevo Lider de Calle']) !!}
					</div>
					<div class="form-group fix-form-group">
					<select class="form-control" name="nombre">
						@foreach($vereda as $ver)
						<option value="{{$ver->id}}">{{$ver->nombre}}</option>
						@endforeach
					</select>
					<!--{!! Form::text('vereda',null,['class'=>'form-control','placeholder'=>'nombre de la vereda']) !!}-->
					</div>
					<button type="submit" class="fix-bottom btn btn-danger">Agregar</button>
					{!! Form::close() !!}
				</div>
			</div>
			@unless(!isset($mensaje))
				<div class="panel-heading">{{$mensaje}}</div>
			@endunless
		</div>	
@endsection