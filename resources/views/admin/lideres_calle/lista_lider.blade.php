@extends('admin.template.main')
@section('title','Listado de lideres de calle')

@section('content')
<div class="col-md-10 col-md-offset-1">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h2>
       Lideres de Calle
   </h2>
</div>
				<table class="table table-striped table-condensed task-table">
					<thead>
					<tr>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Vereda</th>
						<th>Sector</th>
						<th>Parroquia</th>
					</tr>
				</thead>
				@foreach($lideres as $lider)
					<tr>
						<td class="table-text">
							<div>{{$lider->nombre}}</div>
						</td>
						<td class="table-text">
							<div>{{$lider->apellido}}</div>
						</td>
						<td class="table-text">
							<div>{{$lider->vereda}}</div>
						</td>
						<td class="table-text">
							<div>{{$lider->sector}}</div>
						</td>
						<td class="table-text">
							<div>{{$lider->parroquia}}</div>
						</td>
					</tr>
				@endforeach
				</table>
			</div>
		</div>		 	
	</div>
</div>

@endsection