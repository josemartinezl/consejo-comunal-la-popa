<html>
<!--><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"></!-->
<!--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"><link rel="stylesheet" href="/css/app.css">-->
<head>
	 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

  		

  		<link rel="stylesheet"  href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}">

        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
        <!-- Datepicker Files -->
	    <link rel="stylesheet" href="{{asset('datePicker/css/bootstrap-datepicker3.css')}}">
	    <link rel="stylesheet" href="{{asset('datePicker/css/bootstrap-datepicker.standalone.css')}}">
	    
    
</head>
	<header>
		<title>Servicio Comunitario</title>
	</header>
	<body >
		@yield('content')
		

		<script src="{{ asset('plugins/jquery/js/jquery-3.1.1.js') }}"></script>
		<script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>
		<script src="{{asset('datePicker/js/bootstrap-datepicker.js')}}"></script>		
		<!-- Languaje -->
	    <script src="{{asset('datePicker/locales/bootstrap-datepicker.es.min.js')}}"></script>
		@yield('js')
	 </body>
</html>