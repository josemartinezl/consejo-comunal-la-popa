<link rel="stylesheet"  type="text/css" href="css/app.css" > 
<link rel="stylesheet"  type="text/css" href="css/ap.css" > 
<?php $i=1; ?>
<div class="container">
	<div class="row">
		<div class="col-xs-12 text-center">
			<h1>Listado del clap</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<table class="table table-striped table-condensed">
				<tr>
					<th>N°</th>
					<th>Nombre</th>
					<th>Vereda</th>
					<th>Dirección</th>
				</tr>
				@foreach($listas as $listado)
				<tr>
					<td><?php echo $i; $i++; ?></td>
					<td>{{$listado->nombre}}</td>
					<td>{{$listado->vereda}}</td>
					<td>{{$listado->direccion}}</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>
