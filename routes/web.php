<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Parroquia;
use App\Sector;
use App\Vereda;
use App\LiderCalle;
use App\Persona;
use App\Grupo_familiar;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Route::get('/lideres_calle', 'Lideres_calleController@index')->name('lideres_calle.index');
Route::get('/lideres_calle/create', 'Lideres_calleController@create')->name('lideres_calle.create');
Route::post('/lideres_calle', 'Lideres_calleController@store')->name('lideres_calle.store');*/

//aparte

//listado del clap, formato para imprimir
Route::get('/lista_clap', function(Request $request){
	$lista = DB::table('gruposfamiliares')
				->join('veredas','gruposfamiliares.vereda_ID','=','veredas.id')
				->select('gruposfamiliares.nombre as nombre','veredas.nombre as vereda','gruposfamiliares.direccion as direccion')
				->where('veredas.id','=',$request->vereda)
				->get();
	return View('files.lista_clap',['listas'=>$lista]);
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){


	//rutas parroquias
	Route::resource('parroquias','ParroquiasController');
	Route::get('parroquias/{id}/destroy',[ 
		'uses' => 'ParroquiasController@destroy',
		'as' => 'admin.parroquias.destroy'	
		]);

	// ruta sector incompleta
	Route::resource('sectores','SectoresController');

	//rutas paises
	Route::resource('paises','PaisesController');
	Route::get('paises/{id}/destroy',[ 
		'uses' => 'PaisesController@destroy',
		'as' => 'admin.paises.destroy'	
		]);

	// rutas personas
	Route::resource('personas','PersonasController');
	/*
	Route::get('personas/leader',[ 
		'uses' => 'PersonasController@leader',
		'as' => 'admin.personas.leader'	
		]);*/
	// probar la otra forma de nombrar las rutas
	Route::get('/personas/leader', 'PersonasController@leader');

	//Route::get('/leader/leader', 'leader@crear');

	//rutas de lideres de calle

	///Route::get('/lideres_calle', 'Lideres_calleController@index')->name('lideres_calle.index');
	///Route::get('/lideres_calle/create', 'Lideres_calleController@create')->name('lideres_calle.create');
	///Route::post('/lideres_calle', 'Lideres_calleController@store')->name('lideres_calle.store');
	
	//Route::get('/lideres_calle/leader', 'leader@');

	//rutas para busquedas avanzadas
	Route::get('/listado_especial/create', 'AdvancedSearchController@index')->name('listado_especial.index');
	Route::get('/listado_especial/mostrar', 'AdvancedSearchController@mostrar')->name('listado_especial.mostrar');
    //ruta de ajax para las veredas
	Route::get('/getveredas','AdvancedSearchController@mostrar');

	//rutas para el listado de los clap
	Route::get('/listadosClap/create','ListadosClapController@create')->name('listadosclap.create');
	Route::post('/listadosClap', 'ListadosClapController@store')->name('listadosclap.store');
	Route::get('/listadosClap', 'ListadosClapController@index')->name('listadosclap.index');
	Route::get('/listadosClap/{id}/destroy', 'ListadosClapController@destroy')->name('admin.listadosclap.destroy');
	Route::get('/listadosClap/{id}', 'ListadosClapController@show')->name('admin.listadosclap.show');
	Route::get('/listadosClap/{id}/edit', 'ListadosClapController@edit')->name('admin.listadosclap.edit');

	//Manejo de habitantes
	//Route::resource('grupo', 'GrupoController');
	Route::resource('hab', 'HabitanteController');
	Route::get('/resultsearch','HabitanteController@resultsearch')->name('admin.resultsearch');
	Route::get('/buscarhabitante','HabitanteController@buscarhabitante')->name('admin.buscarhabitante');
	Route::get('/buscarpersona','HabitanteController@buscarpersona')->name('admin.buscarpersona');
	Route::get('/crearhab/{id}','HabitanteController@crearhab')->name('admin.crear_habitante');
	Route::get('/indexedit','HabitanteController@indexedit')->name('admin.index_edit');
	Route::get('personas/{id}/destroy','HabitanteController@destroy')->name('admin.hab.destroy');
	Route::get('personas/{id}/activate','HabitanteController@activarhab')->name('admin.hab.activate');
	//--prueba del modal
	Route::get('/indexedit/activar','HabitanteController@ModalEdit')->name('admin.hab.activar');
	Route::get('/indexedit/comprobar','HabitanteController@ModalComprobar');

	//--
	//ruta de ajax para los estados
	Route::get('/getestados','HabitanteController@mostrar');


	//Manejo de grupos familiares 
	Route::get('/grupofamiliar/create','GrupoFamiliarController@create')->name('grupofamiliar.create');
	Route::post('/grupofamiliar','GrupoFamiliarController@store')->name('grupofamiliar.store');
	Route::get('/buscargrupofamiliar','GrupoFamiliarController@buscargrupofamiliar')->name('grupofamiliar.buscar');
	Route::get('/buscargrupofamiliar/mostrar','GrupoFamiliarController@index')->name('grupofamiliar.index');
	Route::get('/grupofamiliar/{id}/edit','GrupoFamiliarController@edit')->name('grupofamiliar.edit');
	Route::get('/grupofamiliar/{id}/destroy','GrupoFamiliarController@destroy')->name('grupofamiliar.destroy');
	Route::put('/grupofamiliar/{id}','GrupoFamiliarController@update')->name('grupofamiliar.update');
	Route::get('/grupofamiliar/{id}/activate','GrupoFamiliarController@activar')->name('grupofamiliar.activate');
    
    //rutas para los lideres de calle
	Route::get('/lideres_listado','Lideres_calleController@listado')->name('lideres.index');
	Route::get('/seccion_lideres',function(){
	$vereda = DB::table('veredas')->get();
	return view('admin.lideres_calle.lista_lideres',['veredas'=>$vereda]);})->name('lideres.find');
	Route::get('/lideres','Lideres_calleController@ver_lider_por_sector');
	Route::get('/nuevolider','Lideres_calleController@lista_veredas')->name('lideres.create');
	Route::resource('lider', 'liderController');

	//solicitud carta de residencia

	Route::get('/solicitar_carta_residencia',function(){
	return view('admin.documentos.carta_resi');
	})->name('carta_resi.index');

	Route::get('solicitud', function(Request $request){
		$band = 0;
		$datos = DB::table('personas')
    				->join('gruposfamiliares','personas.grupo_familiar_id','=','gruposfamiliares.id')
    				->join('veredas','gruposfamiliares.vereda_ID','=','veredas.id')
    				->join('sectores','veredas.sector_ID','=','sectores.id')
    				->select('personas.nombre as nombre','personas.apellido as apellido','personas.cedula as cedula','veredas.nombre as vereda','gruposfamiliares.direccion as direccion','sectores.nombre as sector')
    				->where('personas.cedula','=',$request->cedula)
    				->get();

    	foreach ($datos as $dato) {
    		$band = 1;
    		$pdf = PDF::loadView('files.residencia',['nombre'=>$dato->nombre,
    										'apellido'=>$dato->apellido,
    										'cedula'=>$dato->cedula,
    										'vereda'=>$dato->vereda,
    										'direccion'=>$dato->direccion,
    										'anio'=>2,
    										'sector'=>$dato->sector,
    										'dirigido'=>$request->dirigido,
    										'motivo'=>$request->motivo]);
    		return $pdf->stream();
    	}
    	if($band == 0){
    		return view('admin.documentos.carta_resi',['mensaje'=>'El numero de cedula ingresado no se encuentra registrado...']);
    	}
});

});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home.index');
Route::get('/admin', 'AdminController@index');
