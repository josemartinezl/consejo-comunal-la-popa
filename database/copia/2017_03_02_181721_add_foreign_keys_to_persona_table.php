<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPersonaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('persona', function(Blueprint $table)
		{
			$table->foreign('Grupo_Familiar_ID', 'fk_Persona_Grupo_Familiar1')->references('ID')->on('grupo_familiar')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('PAIS_ID', 'fk_Persona_Pais1')->references('ID')->on('pais')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('persona', function(Blueprint $table)
		{
			$table->dropForeign('fk_Persona_Grupo_Familiar1');
			$table->dropForeign('fk_Persona_Pais1');
		});
	}

}