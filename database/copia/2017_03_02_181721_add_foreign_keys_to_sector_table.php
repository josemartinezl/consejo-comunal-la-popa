<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSectorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sector', function(Blueprint $table)
		{
			$table->foreign('Parroquia_ID', 'fk_Sector_Parroquia1')->references('ID')->on('parroquia')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sector', function(Blueprint $table)
		{
			$table->dropForeign('fk_Sector_Parroquia1');
		});
	}

}