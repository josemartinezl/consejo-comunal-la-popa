<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToGrupoFamiliarTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('grupo_familiar', function(Blueprint $table)
		{
			$table->foreign('Vereda_ID', 'fk_Grupo_Familiar_Vereda1')->references('ID')->on('vereda')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('grupo_familiar', function(Blueprint $table)
		{
			$table->dropForeign('fk_Grupo_Familiar_Vereda1');
		});
	}

}