<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVeredaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vereda', function(Blueprint $table)
		{
			$table->foreign('Sector_ID', 'fk_Vereda_Sector')->references('ID')->on('sector')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vereda', function(Blueprint $table)
		{
			$table->dropForeign('fk_Vereda_Sector');
		});
	}

}