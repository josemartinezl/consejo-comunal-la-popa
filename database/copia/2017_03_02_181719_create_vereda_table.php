<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVeredaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vereda', function(Blueprint $table)
		{
			$table->increments('ID')->primary();
			$table->string('nombre')->unique('nombre_UNIQUE');
			$table->integer('Sector_ID')->index('fk_Vereda_Sector_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vereda');
	}

}