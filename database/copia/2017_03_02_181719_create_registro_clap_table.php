<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRegistroClapTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('registro_clap', function(Blueprint $table)
		{
			$table->integer('CLAP_ID')->index('fk_table1_CLAP1_idx');
			$table->integer('Grupo_Familiar_ID')->index('fk_Registro_clap_Grupo_Familiar1_idx');
			$table->date('fecha');
			$table->string('tipo_mercado')->nullable();
			$table->primary(['CLAP_ID','Grupo_Familiar_ID']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('registro_clap');
	}

}