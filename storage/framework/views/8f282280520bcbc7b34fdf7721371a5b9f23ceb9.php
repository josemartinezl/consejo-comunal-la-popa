<?php $__env->startSection('title','Listado de Personas'); ?>

<?php $__env->startSection('content'); ?>

<div class="col-md-10 col-md-offset-1">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2>
				Resultado de busqueda
			</h2>
		</div>
		<div class="row" style="margin-top: 10px; margin-bottom: 5px;">
			<div class="col-md-1"></div>
			<div class="col-md-4">
				
			
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-1"></div>
			<div class="col-md-4">
			
			</div>
			<div class="col-md-1"></div>
		</div>
		

	</div>
		<table class="table table-striped">
			<thead>

				<th> Cedula </th>
				<th> Nombre </th>
				<th> Apellido </th>
				<th> Telefono </th>
				<th> Acción </th>
			</thead>
			<tbody>
				<?php $__currentLoopData = $personas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $persona): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<tr>
					<td><?php echo e($persona->cedula); ?> </td>
					<td><?php echo e($persona->nombre); ?> </td>
					<td><?php echo e($persona->apellido); ?> </td>
					<td><?php echo e($persona->telefono); ?> </td>
					<td>    <?php if($persona->deleted_at==null): ?>
                                                    <a href="<?php echo e(route('hab.edit', $persona->id)); ?>" class="btn btn-primary"> Editar</a>
                                                    <a href="<?php echo e(route('admin.hab.destroy', $persona->id)); ?>" onclick="return confirm('¿Seguro desea eliminar ? ' )" class="btn btn-danger"> Eliminar </a>
                                                <?php else: ?>
                                                      
                                                      <button  value="<?php echo e($grupo->id); ?>" class="btn btn-primary" type="button" id="activar"  > Activar</button>


                                                <?php endif; ?>
					</td>
				</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</tbody>

		</table>
		<div class="centrar" style="margin-left: 25%">
		<?php echo $personas->render(); ?>

		</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>