<?php $__env->startSection('title','Editar/Eliminar grupo familiar'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-10 col-md-offset-1">
  <div class="panel panel-default">
    <div class="panel-heading">
      <?php if( $familias[0]->deleted_at!=null ): ?>
                        <strong> El grupo familiar <?php echo e($familias[0]->nombre); ?> anteriormente pertenecio a la comunidad pero actualmente no viven en la parroquia </strong>
                    <?php endif; ?>
      <h2>
       Resultados de Busqueda
   </h2>
</div>
<table class="table table-striped table-condensed task-table">



    <thead>

        <th> Familia: <?php echo e($familias[0]->nombre); ?> </th>
        <th> Direccion:  <?php echo e($familias[0]->direccion); ?> </th>
        <th> Vereda: <?php echo e($vereda[0]->nombre); ?> </th>
        <th> 
            <?php if($familias[0]->deleted_at==null): ?>
                                                <a style="height: 25px;
            padding: initial;
            width: auto;" href="<?php echo e(route('grupofamiliar.edit', $familias[0]->id)); ?>" class="btn btn-primary"> Editar</a>
                                                <a  style="height: 25px;
            padding: initial;
            width: auto;" href="<?php echo e(route('grupofamiliar.destroy',$familias[0]->id)); ?>" onclick="return confirm('al eliminar la familia tambien se elimina de la comunidad a sus integrantes ¿ Desea eliminar la familia ? ' )" class="btn btn-danger"> Eliminar </a>
            <?php else: ?>
                    <a  style="height: 25px;
            padding: initial;
            width: auto;" href=" <?php echo e(route('grupofamiliar.activate',$familias[0]->id)); ?> " onclick="return confirm('¿ACtivar grupo familiar  ? ' )" class="btn btn-primary"> Activar </a>
                    <?php endif; ?>
          
        </th>
    </thead>

</table>


<table class="table table-striped table-condensed task-table">

  <thead>
    <th>Nombre </th>
    <th>Apellido </th>
    <th>Cedula</th>
</thead>
<tbody>
    <?php $__currentLoopData = $grupo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $carg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

    <tr>
        <td class="table-text">
            <div><?php echo e($carg->nombre); ?></div>
        </td>

        <td class="table-text">
            <div><?php echo e($carg->apellido); ?></div>
        </td>

        <td class="table-text">
            <div><?php echo e($carg->cedula); ?></div>
        </td>


    </tr>

    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</tbody>
</table>

</div>
</div>      

</div>





<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>