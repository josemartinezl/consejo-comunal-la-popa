<?php $__env->startSection('title','Agregar grupo'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-10 col-md-offset-1">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2>
				Datos de grupo familiar
			</h2>
		</div>
		<?php echo Form::open(['route'=>'grupofamiliar.store','method'=>'POST']); ?>


		<div class="form-group  fix-form-group">
			<?php echo Form::label('vereda_ID','Vereda'); ?>

			<?php echo Form::select('vereda_ID',[''=>'Seleccionar','1'=>'0','2'=>'1','3'=>'2','4'=>'3','5'=>'4','6'=>'5','7'=>'Via Principal Granjas Infatiles','8'=>'Sector Escuela','9'=>'6','10'=>'6 Bis','11'=>'7','12'=>'7 Bis','13'=>'7 Bis Camino Real y 8 Bis','14'=>'8','15'=>'Via Principal Los Tanques','16'=>'Sector Los Tanques'], null,['class'=>'form-control','required']); ?>

		</div>
		<div class="form-group  fix-form-group">
			<?php echo Form::label('nombre','Nombre de Grupo Familiar'); ?>

			<?php echo Form::text('nombreg',null,['class'=>'form-control','placeholder'=>'','required']); ?>

		</div>
		<div class="form-group  fix-form-group">
			<?php echo Form::label('direccion','Direccion de vivienda'); ?>

			<?php echo Form::text('direccion',null,['class'=>'form-control','placeholder'=>'','required']); ?>

		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
		<h2>
			Datosdel Habitante
			</h2>
			</div>

		<div class="form-group  fix-form-group">
			<?php echo Form::label('nombre','Nombre'); ?>

			<?php echo Form::text('nombre',null,['class'=>'form-control','placeholder'=>'','required']); ?>

		</div>

		<div class="form-group  fix-form-group">
			<?php echo Form::label('apellido','Apellido'); ?>

			<?php echo Form::text('apellido',null,['class'=>'form-control','placeholder'=>'','required']); ?>

		</div>

		<div class="form-group  fix-form-group">
			<?php echo Form::label('parentesco','Parentesco'); ?>

			<?php echo Form::text('parentesco',null,['class'=>'form-control','placeholder'=>'Parentesco familiar','required']); ?>

		</div>


		<div class="form-group  fix-form-group">
			<?php echo Form::label('sexo','Sexo'); ?>

			<?php echo Form::select('sexo',[''=>'Seleccion','0'=>'Femenino','1'=>'Masculino'], null,['class'=>'form-control']); ?>

		</div>

		<div class="form-group  fix-form-group">
			<?php echo Form::label('fecha_nacimiento','Fecha Nacimiento'); ?>

			<div class='input-append date datepicker'>
				<?php echo Form::text('fecha_nacimiento',null,['class'=>'form-control','placeholder'=>'Fecha Nacimiento','required']); ?>

				<span class="add-on"><i class="icon-th"></i></span>
			</div>
		</div>
		<div class="form-group fix-form-group">
			<?php echo Form::label('cedula:','Cedula',['class' => 'control-label']); ?>

			<div class="row">
				<div class="col-md-2">
					<?php echo Form::select('tipo',[''=>'Tipo','0'=>'V','1'=>'E','2'=>'P'], null,['class'=>'form-control']); ?>

				</div>
				<div class="col-md-10">
					<?php echo Form::text('cedula',null,['class'=>'form-control','placeholder'=>'Cedula']); ?>

				</div>
			</div>
		</div>

		<div class="form-group  fix-form-group">
			<?php echo Form::label('telefono','Telefono'); ?>

			<?php echo Form::text('telefono',null,['class'=>'form-control','placeholder'=>'']); ?>

		</div>

		<div class="form-group  fix-form-group">
			<?php echo Form::label('estado_civil','Estado Civil'); ?>

			<?php echo Form::text('estado_civil',null,['class'=>'form-control','placeholder'=>'','required']); ?>

		</div>

		<div class="form-group  fix-form-group">
			<?php echo Form::label('jefe_familiar','Jefe Familiar'); ?>

			<?php echo Form::select('jefe_familiar',[''=>'Seleccion','1'=>'Jefe','0'=>'No Jefe'], null,['class'=>'form-control']); ?>

		</div>

		<div class="form-group  fix-form-group">
			<?php echo Form::label('ocupacion','Ocupacion'); ?>

			<?php echo Form::text('ocupacion',null,['class'=>'form-control','placeholder'=>'']); ?>

		</div>

		<div class="form-group  fix-form-group">
			<?php echo Form::label('nivel_instruccion','Nivel de Instruccion'); ?>

			<?php echo Form::text('nivel_instruccion',null,['class'=>'form-control','placeholder'=>'']); ?>

		</div>

		<div class="form-group  fix-form-group">
			<?php echo Form::label('enfermedad','Enfermedad'); ?>

			<?php echo Form::text('enfermedad',null,['class'=>'form-control','placeholder'=>'']); ?>

		</div>

		<div class="form-group  fix-form-group">

			<?php echo Form::label('paises','Pais '); ?>

			<?php echo Form::select('paises', $paises,null,['class' => 'form-control','placeholder'=>'Seleccionar','required']); ?>


		</div>

		<div class="form-group  fix-form-group">	

			<?php echo Form::label('estados','Estado'); ?>

			<?php echo Form::select('estados',['placeholder'=>'Seleccionar Estado'],null,['class' => 'form-control','id'=>'estados']); ?>


		</div>




		<div class="form-group  fix-form-group">
			<?php echo Form::submit('Registrar',['class'=>'btn btn-danger fix-bottom','required']); ?>

		</div>


		<?php echo Form::close(); ?>

		<?php $__env->stopSection(); ?>

		<?php $__env->startSection('js'); ?>
		<script>
			$(document).on('change','#paises',function(){

				var cat_id=$(this).val();

				var div=$(this).parent();

            //var url ="/servicio_comunitario/public/getveredas";
            var op=" ";
            $.ajax({

            	type:'get',
            	url:'<?php echo URL::to('/admin/getestados'); ?>',
            	data:{'id':cat_id},

            	success:function(data){
            		console.log('success');
            		$('#estados').empty();

            		for(var i=0;i<data.length;i++){
            			$('#estados').append(`<option value=${data[i].id}> ${data[i].nombre} </option>`);
            			console.log(data[i].id);
            			console.log(data[i].nombre);
            		}
            	},
            	error:function(){

            	}
            });
        });
    </script>

    <script>
    	$('.datepicker').datepicker({
    		format: "dd-mm-yyyy",
    		language: "es",
    		autoclose: true
    	});
    </script>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>