<link rel="stylesheet"  type="text/css" href="css/app.css" > 
<link rel="stylesheet"  type="text/css" href="css/ap.css" > 
<body>
	<?php $meses[] = [
		'01' => 'Enero',
		'02' => 'Febrero',
		'03' => 'Marzo',
		'04' => 'Abril',
		'05' => 'Mayo',
		'06' => 'Junio',
		'07' => 'Julio',
		'08' => 'Agosto',
		'09' => 'Septiembre',
		'10' => 'Octubre',
		'11' => 'Noviembre',
		'12' => 'Diciembre'
	]; ?>
	<div class="container">
		<div class="row">
			<div class="col-xs-1">
				<img src="images/bicentenario.png">
			</div>
			<div class="col-xs-7">
				<div class="text-center">
					<p><strong>REPÚBLICA BOLIVARIANA DE VENEZUELA<br>
					MINISTERIO DEL PODER POPULAR PARA LAS COMUNAS<br>
					CONSEJO COMUNAL "COLINAS DEL MIRADOR LA POPA"<br>
					PARROQUIA "SAN SEBASTIAN"<br>
					SAN CRISTOBAL - ESTADO TÁCHIRA<br>
					RIF-J-40017834-0</strong></p>
				</div>
			</div>
			<div class="col-xs-2">
				<img src="images/consejo.png">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-12">
				<div class="text-center">
					<p>San Cristóbal <?php echo date('d'); ?> de <?php echo $meses[0][date('m')]; ?> del <?php echo '20'.date('y'); ?></p>
				</div>
				<p><strong>Dirigido:</strong> <?php echo e($dirigido); ?></p>
				<p><strong>Motivo:</strong> <?php echo e($motivo); ?></p>
			</div>

			<h2 class="text-center">Contancia de Residencia</h2>
			<div class="col-xs-12">
						<p>Por medio de la presente, el <strong>Consejo comunal "Colinas del Mirador La Popa"</strong>, ubicados en el Sector La Popa, Parroquia San Sebastian, Municipio San Cristóbal, del Estado Táchira, mediante la presente certificamos que el ciudadano(a): <strong><?php echo e($nombre); ?> <?php echo e($apellido); ?></strong>, mayor de edad, portador de la cedula de identidad: <strong>V</strong> - <strong><?php echo e($cedula); ?></strong>, esta residenciado(a) en la <strong><?php echo e($vereda); ?>, <?php echo e($direccion); ?></strong>, y ha convivido como vecino de nuestra comunidad desde hace <strong><?php echo e($anio); ?></strong> años en el Sector <strong><?php echo e($sector); ?></strong>; para fines legales o a quien pueda interesar.</p>
		
			</div>
		</div>
		<div class="text-center">
			<p>Atentamente</p>
		</div>
		<br><br>
		<div class="row">
			<div class="col-xs-5 text-center">
				_____________________________
			</div>
			<div class="col-xs-5 text-center">
				_____________________________
			</div>
		</div>	
		<div class="row">
			<div class="col-xs-5 text-center">
				Unidad Financiera
			</div>
			<div class="col-xs-5 text-center">
				Unidad de Contraloria
			</div>
		</div>
		<div class="row">
			<div class="col-xs-5 text-center">
				Corina Barrios
			</div>
			<div class="col-xs-5 text-center">
				Maria Gonzaléz
			</div>
		</div>
		<div class="row">
			<div class="col-xs-5 text-center">
				Ced V-21776317
			</div>
			<div class="col-xs-5 text-center">
				Ced V-3193634
			</div>
		</div>
		<br><br>
		<div class="row">
			<div class="col-xs-12 text-center" >
				_______________________________
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 text-center" >
				Vocera de Educación
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 text-center">
				Mercedes Mendez
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 text-center">
				Ced V-11504516
			</div>
		</div>
		<br><br>
		<div class="row">
			<div class="col-xs-12">
				_______________________________________________________________________________________
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 text-center">
				<p><strong>CONSEJO COMUNAL "COLINAS DEL MIRADOR LA POPA"</strong></p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-offset-10 text-center">
				<p>N° 10</p>
			</div>
		</div>
	</div>
</body>