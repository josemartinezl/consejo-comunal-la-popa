<?php $__env->startSection('title','Agregar persona'); ?>

<?php $__env->startSection('content'); ?>
    
        <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                  
                    <div class="panel-heading">
                    <?php $__currentLoopData = $familias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $familia): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <h3><b>Familia <?php echo e($familia->nombre); ?> </b></h3>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
               
                <table class="table table-striped table-condensed task-table">
                                        <thead>
                                            <th>Nombre </th>
                                            <th>Apellido </th>
                                            <th>Cedula</th>
                                        </thead>
                                <tbody>
                                    <?php $__currentLoopData = $grupo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $carg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                   
                                        <tr>
                                            <td class="table-text">
                                                <div><?php echo e($carg->nombre); ?></div>
                                            </td>

                                            <td class="table-text">
                                                <div><?php echo e($carg->apellido); ?></div>
                                            </td>

                                            <td class="table-text">
                                                <div><?php echo e($carg->cedula); ?></div>
                                            </td>
                            
                                            
                                        </tr>
                                        
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                </table>

            </div>
        </div> 
    <div class="col-sm-5 col-sm-offset-2 col-md-6 col-md-offset-5">
                    <?php $__currentLoopData = $familias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $familia): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a href="<?php echo e(route('admin.crear_habitante',$familia->id)); ?>" class="btn btn-danger"> Registrar habitante </a>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        
    </div >
 
     


<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>