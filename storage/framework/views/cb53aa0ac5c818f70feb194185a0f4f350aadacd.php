<?php $__env->startSection('title','Editar/eliminar persona'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-10 col-md-offset-1">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h2>
        Buscar Habitante
      </h2>
    </div>
    <div class="panel-body">
      <div class="form-group">
        <?php echo Form::open(['route'=>'admin.index_edit', 'method'=>'GET','class'=>'navbar-form','role'=>'search']); ?>

        <div class="form-group centrar">
          <?php echo Form::text('nombre',null,['class'=>'form-control','placeholder'=>'Cedula o Nombre']); ?>

        </div>

        <button type="submit" class="btn btn-danger">Ir</button>
        <?php echo Form::close(); ?>

      </div>
                       <!-- <p>
                        <a class="btn btn-info" href="<?php echo e(route('hab.create')); ?>" role="button">Nuevo</a>
                      </p> -->
                    </div>
                  </div>
                </div>
                <?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>