  
  <?php $__env->startSection('title','Listado Especial'); ?>
  <?php $__env->startSection('content'); ?>
  <div class="col-md-10 col-md-offset-1">
  <div class="panel panel-default">
  <div class="panel-heading">
    <h2> Listado Especial </h2>
  </div>
  <?php echo Form::open(['route' => 'listado_especial.index', 'method' => 'GET']); ?>

  <div class="fix-form-group">
    <div class="form-group">
      <?php echo Form::label('estado_civil','Generacion '); ?>

      <?php echo Form::select('generacion',['niños' => 'Menores de edad', 'adultos' => 'Mayores de edad','abuelos' => 'Mayores de 60 años'],null,['class' => 'form-control','placeholder' => 'Seleccione una busqueda de generacion ','required']); ?>

    </div>
    <div class="form-group">
     <?php echo Form::label('parroquias','Parroquia'); ?>

     <?php echo Form::select('parroquias', $parroquias,null,['class' => 'form-control','placeholder' => 'Seleccionar parroquia ','required']); ?>

   </div>
   <div class="form-group"> 
    <?php echo Form::label('vereda','Vereda '); ?>

    <?php if($vereda === null): ?>
    <?php echo Form::select('veredas',['placeholder'=>'Seleccionar vereda'],null,['id'=>'veredas']); ?>

    <?php else: ?>
    <?php echo Form::select('veredas',$vereda ,null,['class'=>'form-control','placeholder'=>'Vereda','required','id'=>'veredas']); ?>

    <?php endif; ?>
  </div>
  </div>
  <div>
  <div class="form-group">
    <?php echo Form::submit('Buscar', ['class' => 'fix-bottom btn btn-danger']); ?>

    </div>
  </div>  
  </div>  
  <table class="table table-striped">
    <thead>
     <th> Cedula </th>
     <th> Nombre </th>
     <th> Apellido </th>
     <th> Fecha de Nacimiento</th>
     <th> Edad </th>
   </thead>
   <tbody>
     <?php $__currentLoopData = $paginado; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $person): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
     <tr>
      <td> <?php echo e($person['cedula']); ?> </td>
      <td> <?php echo e($person['nombre']); ?> </td>
      <td> <?php echo e($person['apellido']); ?> </td>
      <td> <?php echo e($person['fecha_nacimiento']); ?> </td>
      <td> <?php echo e($person['edad']); ?> </td>
    </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </tbody>
  </table>
  <div class="centrar">
  <?php echo e($paginado->render()); ?>

  <?php echo Form::close(); ?>

  </div>
  </div>
  <?php $__env->stopSection(); ?>
  <?php $__env->startSection('js'); ?>
  <script type="text/javascript">
  /* codigo que funciona
  $('#parroquia').on('change', function(){
    console.log('hh');
     var url ="/sapu/public/admin";

      $.ajax({
           

          
          url: url +'/select2-autocomplete-ajax',
      
          success: function(){
              alert('succes');
          },
          error: function(data){
             // console.log(data);
              alert("fail" + ' ' + this.data)
          },

      });
  });
  */
  $(document).on('change','#parroquias',function(){
              // console.log("hmm its change");
              var cat_id=$(this).val();
              var div=$(this).parent();
              var url ="/sapu/public/admin/getveredas";
              var op=" ";
              $.ajax({
                type:'get',
                url:'<?php echo URL::to('/admin/getveredas'); ?>',
                data:{'id':cat_id},
                  //dataType:'json',
                  success:function(data){
                    console.log('success');
                    $('#veredas').empty();
                    if (data == 3){
                      console.log('se metio en la puta comunidad');
                      $('#veredas').append(`<option > Todas las veredas </option>`);
                    }else{
                     for(var i=0;i<data.length;i++){
                       $('#veredas').append(`<option value=${data[i].id}> ${data[i].nombre} </option>`);
                     }
                   }
                 },
                 error:function(){
                 }
               });
            });
          </script>
          <?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>