<?php $__env->startSection('title','Buscar grupo familiar '); ?>
<?php $__env->startSection('content'); ?>

<div class="container">
  <div class="col-md-10 col-md-offset-1">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h2>
          Buscar Grupo Familiar
        </h2>
      </div>
      <div class="panel-body">
       <p class="text-info">Ingresar la cedula de algun habitante que pertenezca al grupo familiar para poder visualizar los integrantes de la familia.</p>  

       <div class="form-group">
        <?php echo Form::open(['route'=>'grupofamiliar.index', 'method'=>'GET','class'=>'navbar-form','role'=>'search']); ?>

        <div class="form-group centrar">
          <?php echo Form::text('nombre',null,['class'=>'form-control','placeholder'=>'Cedula o nombre']); ?>

        </div>
        <button type="submit" class="btn btn-danger">Ir</button>
        <?php echo Form::close(); ?>

      </div>
    </div>
  </div>
</div>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>