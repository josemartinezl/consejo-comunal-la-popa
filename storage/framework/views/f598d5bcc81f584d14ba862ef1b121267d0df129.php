<!DOCTYPE html>
<html lang="en">
<head>
	 <meta charset="utf-8">
	 <title> <?php echo $__env->yieldContent('title','Default'); ?> | Panel de Administracion </title>
	 <link rel="stylesheet"  href="<?php echo e(asset('plugins/bootstrap/css/bootstrap.css')); ?>" >
     <link href="<?php echo e(asset('css/estilo.css')); ?>" rel="stylesheet">
	 <link rel="stylesheet" href="<?php echo e(asset('plugins/datepicker/css/bootstrap-datepicker3.css')); ?>">
     <link rel="stylesheet" href="<?php echo e(asset('plugins/datepicker/css/bootstrap-standalone.css')); ?>">
    
  	
</head>

<body>
<div class="container">
   	<?php echo $__env->make('admin.template.partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('admin.template.partials.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
	<section>
		<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php echo $__env->make('admin.template.partials.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php echo $__env->yieldContent('content'); ?>
	</section>
    </div>
	<script src="<?php echo e(asset('plugins/jquery/js/jquery-3.1.1.js')); ?>"></script>
	<script src="<?php echo e(asset('plugins/bootstrap/js/bootstrap.js')); ?>"></script>
	<script src="<?php echo e(asset('plugins/datepicker/js/bootstrap-datepicker.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/datepicker/locales/bootstrap-datepicker.es.min.js')); ?>"></script>

	<script src="<?php echo e(asset('js/script.js')); ?>"></script>

	 
	<?php echo $__env->yieldContent('js'); ?>

	
</body>
</html