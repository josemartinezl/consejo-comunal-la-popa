<?php $__env->startSection('title','Buscar lider'); ?>


<?php $__env->startSection('content'); ?>
<div class="col-md-10 col-md-offset-1">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2>
				Listado de Lideres de Calle
			</h2>
		</div>
				<div class="panel-body">
					<form action="lideres_listado" class="" role="search">
						<button type="submit" class="btn btn-danger fix-bottom">Ver lista de lideres</button>
					</form>
				</div>
				</div>
				<div class="panel panel-default">
		<div class="panel-heading">
			<h2>
				Lider de Calle por Vereda
			</h2>
		</div>
				<div class="panel-body">
					<form action="lideres" class="" role="search">
					<div class="row">
					<div class="col-md-8">
						<select name="nombre" class="form-control">
							<option value="0">Seleccione</option>
							<?php $__currentLoopData = $veredas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vereda): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($vereda->nombre); ?>"><?php echo e($vereda->nombre); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
						</div>
						<div class="col-md-4">
						<button type="submit" class="btn btn-danger" style="width: 100%">Ver lista de lideres</button>
						</div>
						</div>
					</form>
				</div>
			</div>
		</div>		 	
	</div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>