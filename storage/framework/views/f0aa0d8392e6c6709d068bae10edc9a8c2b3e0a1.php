<?php $__env->startSection('title','Listado Especial'); ?>


<?php $__env->startSection('content'); ?>
<div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h2>
           Jornadas de Clap
          </h2>
        </div>
    	<table class="table table-striped">
		<thead>
			<th> Jornada </th>
			<th> Acción </th>
			
		

		</thead>
		<tbody>
		
			<?php $__currentLoopData = $jornadas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jornada): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<tr>
				<td>  <a href="<?php echo e(route('admin.listadosclap.show', $jornada->fecha)); ?>"> <?php echo e($jornada->fecha); ?> </a>  </td>
				<td>  <a href="<?php echo e(route('admin.listadosclap.destroy', $jornada->fecha)); ?>" onclick="return confirm('¿Seguro quiere eliminar la jornada del clap con fecha del <?php echo e($jornada->fecha); ?> ' )" class="btn btn-danger"> Eliminar</a> </td>
					
				
			
			</tr>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</tbody>
		
	</table>
		<?php echo e($jornadas->render()); ?>




<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>