<?php $__env->startSection('title','Listado Especial'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h2>
            Agregar Jornada de Entrega del CLAP
          </h2>
        </div>

    <?php echo Form::open(['route' => 'listadosclap.store', 'method' => 'POST']); ?>


    <div class="form-group fix-form-group">
        <?php echo Form::label('name','Fecha de jornada'); ?>

        <?php echo Form::text('fecha', \Carbon\Carbon::now(), ['id' => 'datepicker','class' => 'form-control']); ?> 
 
    </div>

    <div class="form-group fix-form-group">

        <?php echo Form::label('mercado','Tipo de Mercado'); ?>

        <?php echo Form::text('mercado',null,['class' => 'form-control', 'placeholder' => 'PDVAL, Mercal, otros' , 'requiere']); ?>

     
    </div>        

    <div class="form-group fix-form-group">
        <?php echo Form::submit('Crear Jornada', ['class' => 'btn btn-danger fix-bottom']); ?>

     
    </div>

    <?php echo Form::close(); ?>


    <?php $__env->startSection('js'); ?>
    
    <script type="text/javascript">
        
        $('#datepicker').datepicker({
            format: "dd-mm-yyyy",
            language: "es",
            autoclose: true
        });
    </script>
    
</div>
</div>
    <?php $__env->stopSection(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>