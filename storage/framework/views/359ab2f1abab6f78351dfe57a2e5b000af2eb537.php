<?php $__env->startSection('title','Listado de lideres de calle'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-10 col-md-offset-1">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h2>
       Lideres de Calle
   </h2>
</div>
				<table class="table table-striped table-condensed task-table">
					<thead>
					<tr>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Vereda</th>
						<th>Sector</th>
						<th>Parroquia</th>
					</tr>
				</thead>
				<?php $__currentLoopData = $lideres; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<tr>
						<td class="table-text">
							<div><?php echo e($lider->nombre); ?></div>
						</td>
						<td class="table-text">
							<div><?php echo e($lider->apellido); ?></div>
						</td>
						<td class="table-text">
							<div><?php echo e($lider->vereda); ?></div>
						</td>
						<td class="table-text">
							<div><?php echo e($lider->sector); ?></div>
						</td>
						<td class="table-text">
							<div><?php echo e($lider->parroquia); ?></div>
						</td>
					</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</table>
			</div>
		</div>		 	
	</div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>