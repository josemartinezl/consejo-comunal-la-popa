<?php $__env->startSection('title','Editar persona'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-10 col-md-offset-1">
    <div class="panel panel-default">
        <div class="panel-heading">
          <h2>
            Modificar Habitante
        </h2>
    </div>
    <?php echo Form::open(['route' => ['hab.update',$persona->id], 'method' => 'PUT']); ?>


    
    <div class="form-group fix-form-group">  
        <?php echo Form::label('nombre','Nombre'); ?>

        <?php echo Form::text('nombre',$persona->nombre,['class'=>'form-control','placeholder'=>'','required']); ?>

    </div>

    <div class="form-group fix-form-group"> 
        <?php echo Form::label('apellido','Apellido'); ?>

        <?php echo Form::text('apellido',$persona->apellido,['class'=>'form-control','placeholder'=>'','required']); ?>

    </div>

    <div class="form-group fix-form-group"> 
        <?php echo Form::label('parentesco','Parentesco'); ?>

        <?php echo Form::text('parentesco',$persona->parentesco,['class'=>'form-control','placeholder'=>'Parentesco familiar','required']); ?>

    </div>


    <div class="form-group fix-form-group"> 
        <?php echo Form::label('sexo','Sexo'); ?>

        <?php echo Form::select('sexo',[''=>'Seleccion','0'=>'Femenino','1'=>'Masculino'], $persona->sexo,['class'=>'form-control','required']); ?>

    </div>

    <div class="form-group fix-form-group"> 
        <?php echo Form::label('fecha_nacimiento','Fecha Nacimiento'); ?>

        <div class='input-append date datepicker'>
            <?php echo Form::text('fecha_nacimiento',$persona->fecha_nacimiento,['class'=>'form-control','placeholder'=>'Fecha Nacimiento','required']); ?>

            <span class="add-on"><i class="icon-th"></i></span>
        </div>
    </div>
    
    <div class="form-group fix-form-group"> 
        <?php echo Form::label('cedula','Cedula'); ?>

        <?php echo Form::text('cedula',$persona->cedula,['class'=>'form-control','placeholder'=>'Cedula']); ?>

    </div>

    <div class="form-group fix-form-group"> 
        <?php echo Form::label('telefono','Teléfono'); ?>

        <?php echo Form::text('telefono',$persona->telefono,['class'=>'form-control','placeholder'=>'']); ?>

    </div>

    <div class="form-group fix-form-group"> 
        <?php echo Form::label('estado_civil','Estado Civil'); ?>

        <?php echo Form::text('estado_civil',$persona->estado_civil,['class'=>'form-control','placeholder'=>'','required']); ?>

    </div>

    <div class="form-group fix-form-group"> 
        <?php echo Form::label('jefe_familiar','Jefe Familiar'); ?>

        <?php echo Form::select('jefe_familiar',[''=>'Seleccion','1'=>'Jefe','0'=>'No Jefe'], $persona->jefe_familia,['class'=>'form-control']); ?>

    </div>

    <div class="form-group fix-form-group"> 
        <?php echo Form::label('ocupacion','Ocupacion'); ?>

        <?php echo Form::text('ocupacion',$persona->ocupacion,['class'=>'form-control','placeholder'=>'']); ?>

    </div>

    <?php echo Form:: hidden('grupo_familiar_id',$persona->grupo_familiar_id); ?>

    
    <div class="form-group fix-form-group"> 
        <?php echo Form::label('nivel_instruccion','Nivel de Instruccion'); ?>

        <?php echo Form::text('nivel_instruccion',$persona->nivel_instruccion,['class'=>'form-control','placeholder'=>'']); ?>

    </div>

    <div class="form-group fix-form-group">     
        <?php echo Form::label('enfermedad','Enfermedad'); ?>

        <?php echo Form::text('enfermedad',$persona->enfermedad,['class'=>'form-control','placeholder'=>'']); ?>

    </div>

    <div class="form-group fix-form-group">  

        <?php echo Form::label('paises','Pais '); ?>

        <?php echo Form::select('paises', $paises,$pais->id,['class' => 'form-control','placeholder'=>'Seleccionar','required']); ?>


    </div>

    <div class="form-group fix-form-group">   

         <?php echo Form::label('estados','Estado'); ?>

        <?php echo Form::select('estados',[$estado->id => $estado->nombre,'placeholder'=>'Seleccionar Estado'],null,['class' => 'form-control','id'=>'estados']); ?>


    </div>
    
    <div>
        <div class="form-group fix-form-group">
        <?php echo Form::submit('Actualizar', ['class' => 'btn btn-danger fix-bottom']); ?>

       </div>
   </div>

   <?php echo Form::close(); ?>


   <?php $__env->stopSection(); ?>  

</div>
</div>
<?php $__env->startSection('js'); ?>
<script>
    $(document).on('change','#paises',function(){

        var cat_id=$(this).val();

        var div=$(this).parent();

            //var url ="/servicio_comunitario/public/getveredas";
            var op=" ";
            $.ajax({

                type:'get',
                url:'<?php echo URL::to('/admin/getestados'); ?>',
                data:{'id':cat_id},

                success:function(data){
                    console.log('success');
                    $('#estados').empty();
                    
                    for(var i=0;i<data.length;i++){
                        $('#estados').append(`<option value=${data[i].id}> ${data[i].nombre} </option>`);
                        console.log(data[i].id);
                        console.log(data[i].nombre);
                    }
                },
                error:function(){

                }
            });
        });
    </script>

    <script>
        $('.datepicker').datepicker({
            format: "dd-mm-yyyy",
            language: "es",
            autoclose: true
        });
    </script>
    <?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>