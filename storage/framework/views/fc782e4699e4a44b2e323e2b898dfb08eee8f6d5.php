<?php $__env->startSection('title','Agregar Lider de Calle'); ?>

<?php $__env->startSection('content'); ?>
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">
          <h2>
            Agregar Lider De Calle
          </h2>
        </div>
				<div class="panel-body">
				<div class="form-group fix-form-group">
				<div class="row">
					<?php echo Form::open(['route'=>'lider.index', 'method'=>'GET','class'=>'','role'=>'search']); ?>

					<div class="form-group fix-form-group">
					<?php echo Form::text('cedula',null,['class'=>'form-control','placeholder'=>'Cedula del Nuevo Lider de Calle']); ?>

					</div>
					<div class="form-group fix-form-group">
					<select class="form-control" name="nombre">
						<?php $__currentLoopData = $vereda; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ver): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<option value="<?php echo e($ver->id); ?>"><?php echo e($ver->nombre); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select>
					<!--<?php echo Form::text('vereda',null,['class'=>'form-control','placeholder'=>'nombre de la vereda']); ?>-->
					</div>
					<button type="submit" class="fix-bottom btn btn-danger">Agregar</button>
					<?php echo Form::close(); ?>

				</div>
			</div>
			<?php if (! (!isset($mensaje))): ?>
				<div class="panel-heading"><?php echo e($mensaje); ?></div>
			<?php endif; ?>
		</div>	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>