<?php $__env->startSection('title','Listado Especial'); ?>


<?php $__env->startSection('content'); ?>
<div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h2>
            Jornada de CLAP con fecha <?php echo e($fecha); ?>

          </h2>
        </div>
    
    	<?php echo Form::open(['route' => ['admin.listadosclap.show',$fecha], 'method' => 'GET','class' => 'navbar-form pull-right']); ?>

		<div class="input-group">
			<?php echo Form::text('nombre',null,['class' => 'form-control','placeholder' => 'Buscar familia...', 'aria-describedby' => 'search']); ?>

			<span class="input-group-addon" id="search"> 
            	<span class="glyphicon glyphicon-search" aria-hidden="true">
            	</span>  
			</span>
			
		</div>
			
	<?php echo Form::close(); ?>


    	<table class="table table-striped">
		<thead>
			<th> Familia </th>
			<th> Dirección </th>
			<th> Vereda </th>
			<th> Estado </th>
			<th> Cambiar </th>		

		</thead>
		<tbody>
		
			<?php $__currentLoopData = $jornadas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jornada): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<tr>
				<td> <?php echo e($jornada->familia); ?>  </td>
				<td> <?php echo e($jornada->direccion); ?>  </td>
				<td> <?php echo e($jornada->vereda); ?>  </td>
				<?php if($jornada->estado === 0): ?>
				<td> <font color="red"> <strong> No entregado </strong> </font> </td>
				<?php else: ?>
				<td> <font color="blue"> <strong> Entregado </strong> </font>   </td>
				<?php endif; ?>

				<td>  <a href="<?php echo e(route('admin.listadosclap.edit', $jornada->id)); ?>" onclick="return confirm('¿Cambiar estado de la familia ? <?php echo e($jornada->familia); ?> ' )" class="btn btn-primary"></a> </td>
			
				
			
			</tr>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</tbody>
		
	</table>

</div>
</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>