<?php $__env->startSection('title','Expedir carta de residencia'); ?>
<?php $__env->startSection('content'); ?>

<div class="col-md-10 col-md-offset-1">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2>
				Solicitar carta de residencia
			</h1>
		</div>
		<div class="panel-body">
			<form method="get" action="solicitud" role="search">

				<div class="form-group fix-form-group">
					<?php echo Form::label('cedula','Cedula del Habitante'); ?>

					<?php echo Form::text('cedula',null,['class'=>'form-control','placeholder'=>'','required']); ?>

				</div>
				<div class="form-group fix-form-group">
					<?php echo Form::label('dirigido','Dirigida a'); ?>

					<?php echo Form::text('dirigido',null,['class'=>'form-control','placeholder'=>'Dirigido a','required']); ?>

				</div>
				<div class="form-group fix-form-group">
				<?php echo Form::label('motivo','Morivo'); ?>

					<?php echo Form::text('motivo',null,['class'=>'form-control','placeholder'=>'Motivo','required']); ?>

				</div>
				<button type="submit" class="btn btn-danger fix-bottom">Generar carta</button>

			</form>
		</div>
		<?php if (! (!isset($mensaje))): ?>
		<div class="panel-heading"><?php echo e($mensaje); ?></div>
		<?php endif; ?>
	</div>
	</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>