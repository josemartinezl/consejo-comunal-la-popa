<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Laracasts\Flash\Flash;
use Carbon\Carbon;
use App\Pais;
use App\Grupo_familiar;
use App\Estado;
use App\Persona;
use App\Http\Requests\PersonaRequest;

class HabitanteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   



        if($request->nombre!=null){

            // busqueda por nombre y apellido

            $grupos = DB::select('select * from personas where CONCAT(CONCAT(nombre," "),apellido) like :busqueda', ['busqueda'=>'%'.$request->nombre.'%']);
            //dd($grupos);
            /*$grupos = DB::table('personas')
                ->where('nombre','like','%'.$request->nombre.'%')
                ->get();*/
           // dd($grupos);
            //if($grupos->count()!=0){

                if($grupos!=null){
               // dd($grupos);
               
                return view('admin.personas.mostrarhabbyname')->with('habitantes',$grupos);
            }else{

                // busqueda por numero de cedula

                $grupos = DB::select('select * from personas where personas.grupo_familiar_id = (SELECT personas.grupo_familiar_id from personas where personas.cedula =  
                                    :ci )', ['ci'=> $request->nombre]);
             
                if($grupos){
                   // dd($grupos);
                    $familias = DB::table('gruposfamiliares')->where('id',$grupos[0]->grupo_familiar_id)->get();

                   \Debugbar::info($familias);
                    return view('admin.personas.mostrarhab',['grupo'=>$grupos,'familias' => $familias]);
                }else{
                    return back();
                }
    
            }
        }else{
            return back();
        }    
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   

        return view('admin.personas.buscarhabitante');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PersonaRequest $request)
    {
       // dd($request->all());

         //$personas = new Persona($request->all());
        //dd($request->all());

        //comprobar que la cedula sea unica y que lleve el tipo de ciudadano... lo ideal es hacerlo por base de datos, eso se vera luego
        $cedula = 0;
        if ($request->tipo != null && $request->cedula != null){

            //dd('se comprobo que ingreso bien los datos');
            
            if ($request->tipo == 0)
            $cedula = 'V' . $request->cedula;
            if ($request->tipo == 1)
            $cedula = 'E' . $request->cedula;    
            if ($request->tipo == 2)
            $cedula = 'P' . $request->cedula;
            
          
                    $result = DB::table('personas')->orderBy('id')->chunk(100, function ($users) use ($cedula) {
                    foreach ($users as $user) {

                        if( $user->cedula == $cedula ){
                            return false;
                        }
                    }

            });

          
         

            if ( $result == false) {
                $paises = DB::table('paises')->select('nombre','id')->orderBy('nombre','ASC')->pluck('nombre','id');
                Flash::error(  "Existe otro habitante con el mismo numero de cedula ");
                return view('admin.personas.crearhabitante',['paises'=>$paises,'grupos'=>$request->grupo_familiar_id]);

            }else{

              //usamos el associate supuestamente para que no haya error al momneto de varias guardados a la vez
                $estado = Estado::find($request->estados);
                $grupo = Grupo_familiar::find($request->grupo_familiar_id);
                $hab = new Persona;
                $hab->nombre= $request->nombre;
                $hab->apellido= $request->apellido;
                $hab->parentesco= $request->parentesco;
                $hab->sexo= $request->sexo;
                $hab->fecha_nacimiento= Carbon::parse($request->fecha_nacimiento)->format('Y-m-d');
                //dd($hab->cedula);
                $hab->cedula= $cedula;
                $hab->telefono= $request->telefono;
                $hab->estado_civil= $request->estado_civil;
                $hab->jefe_familia= $request->jefe_familiar;
                $hab->ocupacion= $request->ocupacion;
                $hab->nivel_instruccion= $request->nivel_instruccion;
                $hab->enfermedad= $request->enfermedad;
                //$hab->estado_id= $request->estados;
                //$hab->grupo_familiar_id=$request->grupo_familiar_id;
                $hab->estado()->associate($estado);
                $hab->grupo_familiar()->associate($grupo);
                $hab->save();
                Flash::success(  $hab->nombre . " " . $hab->apellido . " se ha registrado");
                return view('admin.personas.buscarhabitante');
            }
            
            //dd($result);
            //dd('no encontro igual');


        }
        else
        {
            if ($request->tipo == 3){
                $estado = Estado::find($request->estados);
                $grupo = Grupo_familiar::find($request->grupo_familiar_id);
                $hab = new Persona;
                $hab->nombre= $request->nombre;
                $hab->apellido= $request->apellido;
                $hab->parentesco= $request->parentesco;
                $hab->sexo= $request->sexo;
                $hab->fecha_nacimiento= Carbon::parse($request->fecha_nacimiento)->format('Y-m-d');
                //dd($hab->cedula);
                $hab->cedula= null;
                $hab->telefono= $request->telefono;
                $hab->estado_civil= $request->estado_civil;
                $hab->jefe_familia= $request->jefe_familiar;
                $hab->ocupacion= $request->ocupacion;
                $hab->nivel_instruccion= $request->nivel_instruccion;
                $hab->enfermedad= $request->enfermedad;
                //$hab->estado_id= $request->estados;
                //$hab->grupo_familiar_id=$request->grupo_familiar_id;
                $hab->estado()->associate($estado);
                $hab->grupo_familiar()->associate($grupo);
                $hab->save();
                Flash::success(  $hab->nombre . " " . $hab->apellido . " se ha registrado");
                return view('admin.personas.buscarhabitante');

            }



            $paises = DB::table('paises')->select('nombre','id')->orderBy('nombre','ASC')->pluck('nombre','id');
            Flash::error(  "Debe ingresar bien los datos de la cedula ");
            return view('admin.personas.crearhabitante',['paises'=>$paises,'grupos'=>$request->grupo_familiar_id]);
            //dd($request->tipo);
        }


      
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   

        $persona = Persona::find($id);
        $estado = Estado::find($persona->estado_ID);
        $pais = Pais::find($estado->pais_ID);


        //$pais = DB::table('paises')->where('id')
        $paises = DB::table('paises')->select('nombre','id')->orderBy('nombre','ASC')->pluck('nombre','id');
        return view('admin.personas.edit',['paises'=>$paises,'persona'=>$persona,'pais'=>$pais,'estado'=>$estado]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $hab = Persona::find($id);
        $estado = Estado::find($request->estados);
        $grupo = Grupo_familiar::find($request->grupo_familiar_id);
        //dd($hab);
        
        $hab->nombre = $request->nombre;
        $hab->apellido = $request->apellido;
        $hab->parentesco = $request->parentesco;
        $hab->sexo = $request->sexo;
        $hab->fecha_nacimiento = Carbon::parse($request->fecha_nacimiento)->format('Y-m-d');
        $hab->cedula = $request->cedula;
        $hab->telefono = $request->telefono;
        $hab->estado_civil = $request->estado_civil;
        $hab->jefe_familia = $request->jefe_familiar;
        $hab->ocupacion = $request->ocupacion;
        $hab->nivel_instruccion = $request->nivel_instruccion;
        $hab->enfermedad = $request->enfermedad;

        $hab->estado()->associate($estado);
        $hab->grupo_familiar()->associate($grupo);

        $hab->save();

        Flash::success($hab->nombre. ' ' . $hab->apellido . ' ha sido actualizado'); 

        return view('admin.personas.buscarpersona');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //$persona = Persona::find($id)->delete();
        $persona = Persona::findOrFail($id);
        $persona->delete();
        //dd('ya borre');

        Flash::error($persona->nombre. ' ' . $persona->apellido.' ha sido borrado'); 

        return view('admin.personas.buscarpersona');
        //
    }

    public function buscarhabitante()
    {   
        return view('admin.personas.buscarhabitante');
        //
    }

    public function buscarpersona()
    {   
        return view('admin.personas.buscarpersona');
        //
    }

    public function crearhab($id)
    {   

        $paises = DB::table('paises')->select('nombre','id')->orderBy('nombre','ASC')->pluck('nombre','id');
     
        return view('admin.personas.crearhabitante',['paises'=>$paises,'grupos'=>$id]);
       
        //
    }

    public function mostrar(Request $request){

        $estados = DB::table('estados')->where('pais_iD','=',$request->id)->get();
            
             return response()->json($estados);


    }

    public function indexedit(Request $request){


        if($request->nombre!=null){

            // busqueda por nombre y apellido

            $grupos = DB::select('select * from personas where CONCAT(CONCAT(nombre," "),apellido) like :busqueda', ['busqueda'=>'%'.$request->nombre.'%']);
       

                if($grupos!=null){
               // dd($grupos);
                   
                return view('mostraredit',['grupo'=>$grupos]); 
            }else{

                $grupos = DB::table('personas')->where('cedula','=',$request->nombre)->first();

                
                if($grupos){
                   // dd($grupos);
                   return view('admin.personas.mostraredit')->with('grupo',$grupos);
                }else{
                    return back();
                }
    
            }
        }else{
            return back();
        }    

    }

    public function activarhab($id){

        //Activa el habitante haciendo la columna deleted_at NULL

        $habitante = Persona::onlyTrashed()->findOrFail($id);
        $habitante->restore();
        Flash::success($habitante->nombre. ' ' . $habitante->apellido . ' ha vuelto a la comunidad'); 
        return back();

    }

    public function ModalEdit(Request $request){
        
        $estados = DB::table('personas')->where('id','=',$request->id)->get();
        return response()->json($estados);
        /*
        if($request->ajax()){
            \Debugbar::info('ajax  funcionaa');
            $response = [ 'id' => '12345'];
            return Response::json($response);
        }
        else{
            \Debugbar::info('ajax no funcionaa');
            $grupos = DB::table('personas')->where('cedula','=',$request->cedula)->first();
            return view('admin.personas.mostraredit')->with('grupo',$grupos);

    }*/

 
}

    public function ModalComprobar(Request $request){
        
        if($request->tipo == "comprobar"){
            //comprobar que el propio que va meter no sea uno que no viva en la comunidad
            $persona = DB::table('personas')->where('cedula','=',$request->cedula)->get();
            return response()->json($persona);
        }
        else
        {
            $grupo = Persona::where('cedula','=',$request->cedula)->first();
                if($grupo){
                   
                    
                    DB::table('personas')->where('id', $request->id)->update(['deleted_at' => null,'grupo_familiar_id' => $grupo->grupo_familiar_id]);                   
                  //  $persona = Persona::onlyTrashed()->where('id','=',$request->$id);
                    //return response()->json('De vuelta a la comunidad');
                    
                    //$persona->grupo_familiar()->associate($grupo->grupo_familiar_id);
                    //$persona->save();
                    //$persona->restore();
                    return response()->json('De vuelta a la comunidad');
                }
                else
                {
                     return response()->json('La persona se encuentra desabilitada');
                }    


            
        }


    }

    public function resultsearch(Request $request)
    {
     
        $personas = Persona::Search($request->cedula)->orderBy('id','ASC')->paginate(100);
        return view('admin.personas.resultsearch')->with('personas',$personas);


        
       // return view('admin.personas.index');
        //
    }
}
