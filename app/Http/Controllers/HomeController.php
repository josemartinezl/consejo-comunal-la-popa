<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

       //user = Auth::user();
       //dd($user->name);
       // return redirect()->route('paises.index');
        //return redirect()->route('listado_especial.index');
        return view('admin.template.inicio');
        //return view('admin.template.partials.header');
        //return redirect('/home');
        //return view('admin.listado_especial.index');
    }
}
