<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Persona;
use App\Vereda;
use App\grupo_familiar;
use Illuminate\Support\Facades\DB;

class Lideres_calleController extends Controller
{
    
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         dd('esto es el index');
        //
    }

  

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.lideres_calle.create'); 
    }

       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     	/*
     	me trae un collection
     	$persona = DB::table('personas')->where('cedula',$request->cedula)->get();
     	*/
     	
     	/*
     	me trae los valores directos
     	$persona = DB::table('personas')->where('cedula',$request->cedula)->first();
        */

        $personas = DB::table('personas')->where('cedula',$request->cedula)
     					->join('grupo_familiar','personas.grupo_familiar_id','=','grupo_familiar.id')
     					->join('vereda','grupo_familiar.vereda_id','=','vereda.id')
     					->select('personas.id as perid','vereda.id as verid')->get();
     

     	if($personas->isNotEmpty()){
             	
        	foreach ($personas as $persona) {
    			$person = Persona::find($persona->perid);
    			$vereda = Vereda::find($persona->verid);
                /* para guardar cuando la tabla pivote no tenga campos adicionales 
    			$person->veredas()->attach($vereda); 
                */

                // guardar cuando la clase pivote tenga campos adicionales
                $person->veredas()->attach($vereda,['numeros_familias' => $request->numeros_familias] );
    			
                // guardar cuando la clase pivote tenga campos adicionales
    			//$person->veredas()->save($vereda,['numeros_familias' => $request->numeros_familias] );
    			



    			dd('Persona guardada exitosamente');
			}     	
     	 
     	 //dd('Si existe el habitante');
		}else{
     		dd('no existe el habitante');

     	}
	}

    /* funcion que sirve para mostrar el listado de los lideres de calle actuales
    */
    public function listado()
    {

        $lider = DB::select('select personas.cedula as cedula, personas.nombre as nombre, personas.apellido as apellido, veredas.nombre as vereda, sectores.nombre as sector, parroquias.nombre as parroquia, max(lider_calle.fecha_inicio) from parroquias join sectores on(parroquias.id=sectores.parroquia_ID) join veredas on(sectores.id=veredas.sector_ID) join lider_calle on(veredas.id=lider_calle.vereda_ID) join personas on(lider_calle.persona_ID=personas.id) group by veredas.nombre,personas.cedula, personas.nombre,personas.apellido,sectores.nombre,parroquias.nombre');

        return view('admin.lideres_calle.lista_lider',['lideres'=>$lider]); //hacer pantalla
    }

    /* funcion que sirve para mostrar un listado*/
    
    public function lista_vereda()
    {
        $lista = DB::table('veredas')
                    ->select('nombre')
                    ->get();

        foreach ($lista as $vereda) {
            echo $vereda->nombre.'<br>';
        }
    }

    /* funcion que sirve para mostrar un lider de calle segun la vereda seleccionada*/

    public function ver_lider_por_sector(Request $request)
    {

        $lider = DB::select('select personas.cedula as cedula, personas.nombre as nombre, personas.apellido as apellido, veredas.nombre as vereda, sectores.nombre as sector, parroquias.nombre as parroquia, max(lider_calle.fecha_inicio) from parroquias join sectores on(parroquias.id=sectores.parroquia_ID) join veredas on(sectores.id=veredas.sector_ID) join lider_calle on(veredas.id=lider_calle.vereda_ID) join personas on(lider_calle.persona_ID=personas.id) where veredas.nombre = "'.$request->nombre.'" group by veredas.nombre,personas.cedula, personas.nombre,personas.apellido,sectores.nombre,parroquias.nombre');

        return view('admin.lideres_calle.lider',['lider_ac'=>$lider]); //hacer pantalla
    }

    public function lista_veredas(){
        $calles = DB::table('veredas')
                    ->select('id','nombre')
                    ->get();
        return view('admin.lideres_calle.nuevol',['vereda' => $calles]);
    }
   
}
