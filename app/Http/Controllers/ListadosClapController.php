<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grupo_familiar;
use App\Clap;
use App\Persona;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;
use Carbon\Carbon; 

class ListadosClapController extends Controller
{


      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jornadas = DB::table('registro_clap')->select('fecha')->distinct()->simplepaginate(5);

          \Debugbar::info($jornadas->items());

     

        return view('admin.listadosClap.index')->with('jornadas',$jornadas);
        //
    }



	 /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.listadosClap.create');
        //
    }


       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


       
        $clap = Clap::find(1);
        $grupos = Grupo_familiar::all();
        //$fecha = Carbon::now();
        $fecha = Carbon::parse($request->fecha)->format('Y-m-d');
       
        foreach ($grupos as $grupo) {
          
            $clap->grupos()->attach($grupo,['fecha' => $fecha , 'tipo_mercado' => $request->mercado,'estado' => 0] );
        }

        return redirect()->route('listadosclap.index');
      

        //
    }

        /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        //dd($request->nombre);
        if ($request->nombre == null){
            \Debugbar::info($request);
            $jornada = DB::table('registro_clap')->where('fecha', $id)
                    ->join('gruposfamiliares','gruposfamiliares.id','=','registro_clap.grupo_familiar_id')
                    ->join('veredas','gruposfamiliares.vereda_id','=','veredas.id')
                    ->select('gruposfamiliares.nombre as familia','gruposfamiliares.direccion','veredas.nombre as vereda','registro_clap.estado','registro_clap.id')->get();            

            return view('admin.listadosclap.show',['jornadas' => $jornada , 'fecha' => $id]);

        }else{
           //  dd('no es nulo');
            $persona = Persona::Search($request->nombre)->first();
            \Debugbar::info($persona);
            $jornada = DB::table('registro_clap')->where([
                                                        ['fecha', '=', $id],
                                                        ['grupo_familiar_id', '=', $persona->grupo_familiar_id],])
                    ->join('gruposfamiliares','gruposfamiliares.id','=','registro_clap.grupo_familiar_id')
                    ->join('veredas','gruposfamiliares.vereda_id','=','veredas.id')
                    ->select('gruposfamiliares.nombre as familia','gruposfamiliares.direccion','veredas.nombre as vereda','registro_clap.estado','registro_clap.id')->get(); 

           // dd($persona->grupo_familiar_id);
            return view('admin.listadosclap.show',['jornadas' => $jornada , 'fecha' => $id]);

        }
       


        
    }

      /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      //  $pais = Pais::find($id);
        //$pais->delete();
        //$deletedRows = App\Flight::where('active', 0)->delete();
        DB::table('registro_clap')->where('fecha', $id)->delete();
        Flash::error('La jornada con fecha del ' . $id . ' ha sido borrada'); 

        return redirect()->route('listadosclap.index');
        //
    }


    public function edit($id){

        DB::table('registro_clap')->where('id', $id)->update(['estado' => 1]);

        $fecha = DB::table('registro_clap')->where('id', $id)->value('fecha');
          
        return redirect()->route('admin.listadosclap.show',$fecha);
        


    }


    //
}
