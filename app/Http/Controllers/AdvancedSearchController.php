<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Parroquia;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Carbon\carbon;


class AdvancedSearchController extends Controller
{

	  
    public function index(Request $request)
    {


      		//dd($request);

    	    $parroquias = DB::table('parroquias')->select('nombre','id')->orderBy('nombre','ASC')->pluck('nombre','id');
    	    $nombre_vereda = DB::table('veredas')->where('id',$request->veredas)->pluck('nombre','id');

    	    if ($request->parroquias != 3){ 
    	   	
    	   	$personas = DB::table('veredas')->where('vereda_id',$request->veredas)
             			  ->join('gruposfamiliares','gruposfamiliares.vereda_id','=','veredas.id')
    	    			  ->join('personas','gruposfamiliares.id','=','personas.grupo_familiar_id')
    	    			  ->select('personas.nombre','personas.apellido','personas.cedula','personas.fecha_nacimiento','veredas.nombre as vereda')->get();

    	   
    	    
    	    
    	    	//dd($nombre_vereda);
			}
  	    		
    	    if ($request->parroquias == 3 ) {

    	    	$personas = DB::table('personas')->get();
    	 
              }			
 
             
			if ($personas->isNotEmpty()){
 			//dd($request);
 			\Debugbar::info($personas);
 		
			$data = [];
			foreach ($personas as $persona) {
    			
				  $edad = new Carbon($persona->fecha_nacimiento);
				  if ($request->generacion == 'adultos' && ($edad->age >= 18 && $edad->age < 60) ){
				  	  
				  	  		  	   $data [] = array ('nombre'=>$persona->nombre,
                 									 'apellido'=>$persona->apellido,
                 									 'cedula' =>$persona->cedula,
                 									 'fecha_nacimiento'=>$persona->fecha_nacimiento,
                 									 'edad'=>$edad->age );
				  	  		  	    
			
				  }


				  if ($request->generacion == 'niños' && ($edad->age >= 0 && $edad->age < 18) ){
				  	  
				  	  		  	   $data [] = array ('nombre'=>$persona->nombre,
                 									 'apellido'=>$persona->apellido,
                 									 'cedula' =>$persona->cedula,
                 									 'fecha_nacimiento'=>$persona->fecha_nacimiento,
                 									 'edad'=>$edad->age);
				  	  		  	    
			
				  }

				  if ($request->generacion == 'abuelos' && $edad->age >= 60 ){
				  	  
				  	  		  	   $data [] = array ('nombre'=>$persona->nombre,
                 									 'apellido'=>$persona->apellido,
                 									 'cedula' =>$persona->cedula,
                 									 'fecha_nacimiento'=>$persona->fecha_nacimiento,
                 									 'edad'=>$edad->age);
				  	  		  	    
			
				  }

			}
			//pasar el array a collection	 			}
			$persona = collect($data);
			\Debugbar::info($persona);
	     	// items que quiero por paja	
			$per_page = 10;
			\Debugbar::info(' se mete');
			// pagina actual pero no se vuelve loco
   			$current_page = isset($request->page) ? (int)$request->page : 1;
   			//splitear el collection para mostrar en cada pagina
    		$persona = $persona->slice((($current_page-1) * $per_page), $per_page);
    		$paginado = new Paginator($persona,$per_page,Paginator::resolveCurrentPage() ); 
            // para las url del paginator
    		$paginado = $paginado->setPath($request->url());
			$paginado->appends($request->except(['page']));	 
			//\Debugbar::info( $paginado->currentPage());

			
			if ($paginado->count()==$per_page) {
				$paginado->hasMorePagesWhen(true);
				//arreglar esto
			}
	    }
			else {
		   
				\Debugbar::info('no se mete');
				$paginado = new Paginator($personas,0,0); 
			}
		//dd($nombre_vereda);
		\Debugbar::info($nombre_vereda);  
    	return view('admin.listado_especial.index',['parroquias' => $parroquias, 'paginado' => $paginado, 'vereda' => $nombre_vereda ]);
        //
    }

      public function mostrar(Request $request)
    {
    	if($request->id ==  3){
    		return response()->json($request->id);
    	}else{
    		$sector = DB::table('sectores')->where('parroquia_id','=',$request->id)->first();
        	$veredas = DB::table('veredas')->where('sector_id','=',$sector->id)->get();
        	
    		 return response()->json($veredas);
    		}
    	
    }



    //
}
