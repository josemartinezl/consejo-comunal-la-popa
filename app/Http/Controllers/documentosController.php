<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
use App\Parroquia;
use App\Sector;
use App\Vereda;
use App\LiderCalle;
use App\Persona;
use App\Grupo_familiar;
use Illuminate\Support\Facades\DB;

class documentosController extends Controller
{
    public function index(Request $request){
    	$datos = DB::table('personas')
    				->join('gruposfamiliares','personas.grupo_familiar_id','=','gruposfamiliares.id')
    				->join('veredas','gruposfamiliares.vereda_ID','=','veredas.id')
    				->join('sectores','veredas.sector_ID','=','sectores.id')
    				->select('personas.nombre as nombre','personas.apellido as apellido','personas.cedula as cedula','veredas.nombre as vereda','gruposfamiliares.direccion as direccion','sectores.nombre as sector')
    				->where('personas.cedula','=',$request->cedula)
    				->get();
    	foreach ($datos as $dato) {
    		return View('files.residencia',['nombre'=>$dato->nombre,
    										'apellido'=>$dato->apellido,
    										'cedula'=>$dato->cedula,
    										'vereda'=>$dato->vereda,
    										'direccion'=>$dato->direccion,
    										'anio'=>2,
    										'sector'=>$dato->sector]);
    	}
    	
    	return $pdf->stream();
    }

}
/* function(){
	pdf = PDF::loadView('files.residencia');$
	//return $pdf->download('carta_residencia.pdf'); //para descargar
	return $pdf->stream(); //para ver el documento
	//return view('files.residencia');
}*/