<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
use App\Parroquia;
use App\Sector;
use App\Vereda;
use App\LiderCalle;
use App\Persona;
use Illuminate\Support\Facades\DB;

class liderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {	

    	if($request->cedula != null)
    	{
    		$calles = DB::table('veredas')
                    ->select('id','nombre')
                    ->get();

            $lugar = DB::table('veredas')
            		->select('nombre')
            		->where('id','=',$request->nombre)
            		->get();

            $lugarv = '';
            foreach ($lugar as $verl) {
            	$lugarv = $verl->nombre;
            }

	    	$persona = DB::table('personas')
	    				->join('gruposfamiliares','personas.grupo_familiar_id','=','gruposfamiliares.id')
	    				->join('veredas','gruposfamiliares.vereda_ID','=','veredas.id')
	    				->select('veredas.id as vereda','personas.id as persona','personas.nombre as nombre','veredas.nombre as nombre_vereda')
	    				->where('personas.cedula','=',$request->cedula)
	    				->get();
	    	
	    	$cantidad = DB::table('lider_calle')
	    					->select('n_familia')
	    					->where('vereda_ID','=',$request->nombre)
	    					->get();
	    	$cantidad_f = 0;

	    	foreach ($cantidad as $familias) {
	    		$cantidad_f = $familias->n_familia;
	    		//echo $familias->n_familia;
	    	}

	    	foreach ($persona as $codigo) {

	    		if($codigo->vereda == $request->nombre){
					DB::table('lider_calle')->insertGetId(['n_familia' => $cantidad_f,'fecha_inicio' => date('d-m-y h:i'),'vereda_ID'=>$codigo->vereda,'persona_ID'=>$codigo->persona,'created_at' => date('d-m-y h:i'),'updated_at' => date('d-m-y h:i')]);
	    			return view('admin.lideres_calle.nuevol',['mensaje'=>'Se ha agregado la informacion del nuevo lider de calle exitosamente','vereda' => $calles]);
	    		}else{
	    			return view('admin.lideres_calle.nuevol',['mensaje'=>'El señor@ '.$codigo->nombre.' no pertenece a la '.$lugarv,'vereda' => $calles]);
	    		}
	    	}
	    }
	    else
	    {
	    	return back();
	    }

	   // echo 'la vereda'.$request->nombre;
    }
}
