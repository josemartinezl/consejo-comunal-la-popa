<?php

namespace App\Http\Controllers;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Laracasts\Flash\Flash;
use Carbon\Carbon;
use App\Http\Requests\PersonaRequest;
use App\Pais;
use App\Vereda;
use App\Grupo_familiar;
use App\Estado;
use App\Persona;

class GrupoFamiliarController extends Controller
{



	public function buscargrupofamiliar(){
		return view('admin.grupofamiliar.buscargrupofamiliar');

	}

	public function index(Request $request){

	  if($request->nombre!=null){

            // busqueda por nombre y apellido... arreglar para tire el grupo familiar

            $grupos = DB::select('select * from personas where CONCAT(CONCAT(nombre," "),apellido) like :busqueda', ['busqueda'=>'%'.$request->nombre.'%']);
            if($grupos!=null){
               // dd($grupos);
                   
                return view('mostrarhab',['grupo'=>$grupos]); 
            }else{

                // busqueda por numero de cedula

                $grupos = DB::select('select * from personas where personas.grupo_familiar_id = (SELECT personas.grupo_familiar_id from personas where personas.cedula = :ci )', ['ci'=> $request->nombre]);
             
                if($grupos){
                   // dd($grupos);
                    $familias = DB::table('gruposfamiliares')->where('id',$grupos[0]->grupo_familiar_id)->first();
                   	$vereda = DB::table('veredas')->where('id',$familias->vereda_ID)->first();
                   	\Debugbar::info($familias);
                   
                    return view('admin.grupofamiliar.mostrargrupo',['grupo'=>$grupos,'familias' => array($familias) ,'vereda' => array($vereda)]);
                }else{
                    return back();
                }
    
            }
        }else{
            return back();
        }   

	}
	 /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   

    	$paises = DB::table('paises')->select('nombre','id')->orderBy('nombre','ASC')->pluck('nombre','id');
        return view('admin.grupofamiliar.agregargrupo')->with('paises',$paises);
        //
    }


    //

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	
        $validator = Validator::make($request->all(), [
        	'nombreg' => 'required|min:4|max:255',
			'direccion' => 'required|min:4|max:255',
			'nombre' => 'required|min:4|max:255',
            'apellido' => 'required|min:4|max:255',
            'parentesco' => 'required|min:4|max:255',
             //'cedula' => 'required|digits_between:7,8|unique:personas',
            'fecha_nacimiento' => 'required|date|before:today'
        ]);

         if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }   

         //comprobar que la cedula sea unica y que lleve el tipo de ciudadano... lo ideal es hacerlo por base de datos, eso se vera luego
        $cedula = 0;
        if ($request->tipo != null && $request->cedula != null){

            //dd('se comprobo que ingreso bien los datos');
            if ($request->tipo == 0)
            $cedula = 'V' . $request->cedula;
            if ($request->tipo == 1)
            $cedula = 'E' . $request->cedula;    
            if ($request->tipo == 2)
            $cedula = 'P' . $request->cedula;

            $result = DB::table('personas')->orderBy('id')->chunk(100, function ($users) use ($cedula) {
            foreach ($users as $user) {

                if( $user->cedula == $cedula ){
                    return false;
                        }
                    }

            });

            if ( $result == false) {
                $paises = DB::table('paises')->select('nombre','id')->orderBy('nombre','ASC')->pluck('nombre','id');
                Flash::error(  "Existe otro habitante con el mismo numero de cedula ");
                return view('admin.personas.crearhabitante',['paises'=>$paises,'grupos'=>$request->grupo_familiar_id]);

            }else{


                    //dd($request->all());
                    $grupo= new Grupo_familiar;
                    $vereda = Vereda::find($request->vereda_ID);
                    $grupo->nombre = $request->nombreg;
                    $grupo->direccion = $request->direccion;
                    $grupo->vereda()->associate($vereda);
                    $grupo->save();

                    $estado = Estado::find($request->estados);
                    $hab = new Persona;
                    $hab->nombre= $request->nombre;
                    $hab->apellido= $request->apellido;
                    $hab->parentesco= $request->parentesco;
                    $hab->sexo= $request->sexo;
                    $hab->fecha_nacimiento= Carbon::parse($request->fecha_nacimiento)->format('Y-m-d');
                    $hab->cedula= $cedula;
                    $hab->telefono= $request->telefono;
                    $hab->estado_civil= $request->estado_civil;
                    $hab->jefe_familia= $request->jefe_familiar;
                    $hab->ocupacion= $request->ocupacion;
                    $hab->nivel_instruccion= $request->nivel_instruccion;
                    $hab->enfermedad= $request->enfermedad;
                    $hab->estado()->associate($estado);
                    $hab->grupo_familiar()->associate($grupo);
                    $hab->save();

                    Flash::success(  "La familia ". $grupo->nombre . " se ha registrado");
                    $paises = DB::table('paises')->select('nombre','id')->orderBy('nombre','ASC')->pluck('nombre','id');
                    return view('admin.grupofamiliar.agregargrupo')->with('paises',$paises);

            }    
          
 
            }else{

                $paises = DB::table('paises')->select('nombre','id')->orderBy('nombre','ASC')->pluck('nombre','id');
                Flash::error(  "Debe ingresar bien los datos de la cedula ");
                return view('admin.grupofamiliar.agregargrupo',['paises'=>$paises]);
        }

        

    }

    public function edit($id){

    	$grupos = DB::table('gruposfamiliares')->where('id',$id)->first();
    	//$grupos = Grupo_familiar::find($id);
    	//DD($grupos);
    	return view('admin.grupofamiliar.edit')->with('grupos',array($grupos));


    }
    
    public function update(Request $request, $id){   

    	$grupo = Grupo_familiar::find($id);
    	$vereda = Vereda::find($request->vereda_ID);
    	$grupo->nombre = $request->nombre;
    	$grupo->direccion = $request->direccion;
    	$grupo->vereda()->associate($vereda);
    	$grupo->save();

    	Flash::success(  "La familia ". $grupo->nombre . " se ha actualizado");
    	return view('admin.grupofamiliar.buscargrupofamiliar');


    	
    	
    }

    public function destroy($id){
         
         //Desactiva el grupo familiar sin eliminar de la base de datos dandole a la columna deleted_at la fecha actual
        $grupo = Grupo_familiar::findOrFail($id);
      
        $familia=$grupo->personas;
        foreach ($familia as $fam) {
        $habitante=Persona::findOrFail($fam->id);
        $habitante->delete();  
        }
        $grupo->delete();
       
        Flash::error($grupo->nombre. " ".'ha sido borrado'); 

        return view('admin.grupofamiliar.buscargrupofamiliar');

    	//queda pendiente ya que para eliminar un grupofamiliar hay que eliminar las personas con clave foranea de dicho grupo familiar
    	//sotfdelete es la solucion

    }


    public function activar($id){

        //Activa el grupo familia haciendo la columna deleted_at NULL
      
        $flights = Grupo_familiar::onlyTrashed()->findOrFail($id);
        $flights->restore();
        \Debugbar::info('se metio');
         
 
        $familia = Persona::withTrashed() ->where('grupo_familiar_id', $id) ->get();
        foreach ($familia as $fam) {
        $flights = Persona::onlyTrashed()->findOrFail($fam->id);
        $flights->restore(); 
        }
             return back();

    }

}
