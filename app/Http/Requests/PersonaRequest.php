<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'nombre' => 'required|min:4|max:255',
            'apellido' => 'required|min:4|max:255',
            'parentesco' => 'required|min:3|max:255',
            'sexo' => 'required',
            'estado_civil' => 'required',
            'fecha_nacimiento' => 'required|date|before:today'




            //
        ];
    }
}
