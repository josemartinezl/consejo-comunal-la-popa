<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
//use Illuminate\Contracts\View\View;
use App\Persona;
use App\Grupo_familiar;

class StatsComposer
{
    

 
    public function __construct()
    {
       
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $count_users = Persona::all()->count();
        $count_family = Grupo_familiar::all()->count();
        $array = [
            "count_users" =>   $count_users,
            "count_family" => $count_family,
        ];
        $view->with('stats',$array);
    }
}