<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Record_clap extends Pivot
{

	protected $table = "registro_clap";
	protected $fillable = ['tipo_mercado','fecha','estado','clap_id','grupo_familiar_id'];

	 protected $dates = [
        'fecha'
    ];
    //
}
