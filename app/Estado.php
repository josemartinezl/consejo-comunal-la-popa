<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{

	protected $table = "estados";
	protected $fillable = ['nombre','pais_id'];
    //


    public function personas(){
	
		return $this->hasMany('App\Persona');
	}

	public function pais(){
		return $this->belongsTo('App\Pais');
	}


	public function scopeSearch($query, $nombre){

		return $query->where('nombre', 'LIKE',"%$nombre%");

	}
    //
}
