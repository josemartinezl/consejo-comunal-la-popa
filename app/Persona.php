<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot; 
use Illuminate\Database\Eloquent\SoftDeletes;

class Persona extends Model
{
	protected $table = "personas";
	protected $fillable = ['nombre','apellido','parentesco','cedula','telefono','fecha_nacimiento','estado_civil','sexo',
						   'enfermedad','nivel_instruccion','ocupacion','grupo_familiar_id','lugar','estado_id','jefe_familia'];

	Use SoftDeletes;	
	protected $dates = [
         'created_at',
       	 'updated_at',
       	 'deleted_at',
       	 'fecha_nacimiento'
    ];



	public function grupo_familiar(){
		return $this->belongsTo('App\Grupo_familiar');
	}


	public function estado(){
		return $this->belongsTo('App\Estado');
	}

	public function veredas(){
		return $this->belongsToMany('App\Vereda','lideres_calle')
			   ->using('App\Lider_calle')
			   ->withPivot('fecha_inicio','fecha_fin','numeros_familias')->withTimestamps();
	}


    public function scopeSearch($query, $cedula){

		//return $query->where('cedula',$cedula);
		return $query->where('cedula', 'LIKE',"%$cedula%");

	}

}
