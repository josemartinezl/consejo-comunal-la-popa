<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\SoftDeletes;

class Grupo_familiar extends Model
{
    Use SoftDeletes;
	protected $table = "gruposfamiliares";
	protected $fillable = ['nombre','direccion','vereda_id'];
    //
    
    public function personas(){
    	return $this->hasMany('App\Persona');
    }

    public function vereda(){

    	return $this->belongsTo('App\Vereda');
    }


    public function claps(){
	
	return $this->belongsToMany('App\Clap','registro_clap')
			->using('App\Record_clap')
			->withPivot('fecha','tipo_mercado','estado');
	}

}
